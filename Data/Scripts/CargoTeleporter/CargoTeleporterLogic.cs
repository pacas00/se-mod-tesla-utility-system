﻿using System;
using System.Collections.Generic;
using System.Linq;

using Sandbox.Common.ObjectBuilders;
using Sandbox.Common.ObjectBuilders.Definitions;
using Sandbox.Definitions;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.EntityComponents;
using Sandbox.Game.Lights;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Weapons;
using SpaceEngineers.Game.ModAPI;
using System.Timers;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.Entity;
using VRage.Game.ModAPI;
using VRage.Game.ModAPI.Interfaces;
using VRage.Game.ObjectBuilders;
using VRage.ObjectBuilders;
using VRage.Library.Utils;
using VRage.ModAPI;
using VRage.Utils;

using Sandbox.ModAPI.Interfaces.Terminal;
using Sandbox.Game.Entities.Inventory;

using IMyFunctionalBlock = Sandbox.ModAPI.IMyFunctionalBlock;
using IMyLaserAntenna = Sandbox.ModAPI.IMyLaserAntenna;
using IMyTerminalBlock = Sandbox.ModAPI.IMyTerminalBlock;

namespace CargoTeleporter
{
    [MyEntityComponentDescriptor(typeof(MyObjectBuilder_ConveyorSorter), new string[] { "LargeBlockCargoTeleportTethered", "SmallBlockCargoTeleportTethered", "SmallBlockCargoTeleportNFC", "LargeBlockCargoTeleportNFC" })]
    public class CargoTeleporterLogic : MyGameLogicComponent
    {
        MyObjectBuilder_EntityBase ObjectBuilder;
        IMyCubeBlock CargoTeleporter = null;
        IMyTerminalBlock terminal = null;
        MyEntity myEnt = null;
        IMyInventory inventory = null;
        bool LoadedData = false;

        static bool controlsAdded = false;
        public static List<IMyTerminalControl> mTermControls = new List<IMyTerminalControl>();
        public override void Init(MyObjectBuilder_EntityBase objectBuilder)
        {
            Entity.NeedsUpdate |= MyEntityUpdateEnum.EACH_100TH_FRAME | MyEntityUpdateEnum.BEFORE_NEXT_FRAME;
            ObjectBuilder = objectBuilder;
            CargoTeleporter = Entity as IMyCubeBlock;
            terminal = (IMyTerminalBlock)Entity;
            myEnt = Entity as MyEntity;
            if (myEnt.HasInventory)
            {
                inventory = myEnt.GetInventoryBase() as MyInventory;
            }
            base.Init(objectBuilder);
        }

        public bool LogicAbort = false;

        public override MyObjectBuilder_EntityBase GetObjectBuilder(bool copy = false)
        {
            return copy ? ObjectBuilder.Clone() as MyObjectBuilder_EntityBase : ObjectBuilder;
        }

        public override void Close()
        {
            LogicAbort = true;
            base.Close();

        }

        static IMyTerminalControlSeparator SeperatorControl;
        static IMyTerminalControlLabel tetherControlLabel;
        static IMyTerminalControlTextbox targetNameControl;
        static IMyTerminalControlCheckbox checkboxControl;
        static IMyTerminalControlLabel checkboxControlLabel;
        static IMyTerminalControlSeparator SeperatorControl2;
        static IMyTerminalControlLabel nfcControlLabel;
        static IMyTerminalControlTextbox nfcPlateNameControl;
        static IMyTerminalControlSlider nfcRangeControl;

        private void AddTerminalControls()
        {
            if (MyAPIGateway.TerminalControls == null)
            {
                Entity.NeedsUpdate |= MyEntityUpdateEnum.BEFORE_NEXT_FRAME;
                return;
            }

            if (controlsAdded) return;
            controlsAdded = true;

            targetNameControl =
                MyAPIGateway.TerminalControls.CreateControl<IMyTerminalControlTextbox, IMyConveyorSorter>(
                    "Pacas00.TargetName");

            if (targetNameControl == null)
            {
                controlsAdded = false;
                Entity.NeedsUpdate |= MyEntityUpdateEnum.BEFORE_NEXT_FRAME;
                return;
            }

            SeperatorControl =
                MyAPIGateway.TerminalControls.CreateControl<IMyTerminalControlSeparator, IMyConveyorSorter>(
                    "Pacas00.Seperator");
            SeperatorControl.Visible = (b) => Definitions.CargoTeleportTethered.Contains(b.BlockDefinition.SubtypeName);
            mTermControls.Add(SeperatorControl);

            tetherControlLabel =
                MyAPIGateway.TerminalControls.CreateControl<IMyTerminalControlLabel, IMyConveyorSorter>("Pacas00.tetherControlLabel");

            tetherControlLabel.Label = MyStringId.GetOrCompute("Tether Settings");
            tetherControlLabel.Visible = (b) => Definitions.CargoTeleportTethered.Contains(b.BlockDefinition.SubtypeName);

            mTermControls.Add(tetherControlLabel);



            targetNameControl.Title = MyStringId.GetOrCompute("Target Name");
            targetNameControl.Getter =
                (b) => new System.Text.StringBuilder(b.GameLogic.GetAs<CargoTeleporterLogic>().targetName);
            targetNameControl.Setter = (b, v) => setTargetName(b, v.ToString());
            targetNameControl.Visible = (b) => Definitions.CargoTeleportTethered.Contains(b.BlockDefinition.SubtypeName);
            mTermControls.Add(targetNameControl);


            checkboxControlLabel =
                MyAPIGateway.TerminalControls.CreateControl<IMyTerminalControlLabel, IMyConveyorSorter>("Pacas00.chkModeLabel");

            checkboxControlLabel.Label = MyStringId.GetOrCompute(" TO Mode -> OFF\n FROM Mode -> ON");
            checkboxControlLabel.Visible = (b) => Definitions.CargoTeleportTethered.Contains(b.BlockDefinition.SubtypeName);

            

            checkboxControl =
                MyAPIGateway.TerminalControls.CreateControl<IMyTerminalControlCheckbox, IMyConveyorSorter>(
                    "Pacas00.chkMode");

            checkboxControl.Title = MyStringId.GetOrCompute("Transfer Mode");
            checkboxControl.Getter = (b) => b.GameLogic.GetAs<CargoTeleporterLogic>().toFromMode;
            checkboxControl.Setter = (b, v) => setBoolMode(b, v);
            checkboxControl.Visible = (b) => Definitions.CargoTeleportTethered.Contains(b.BlockDefinition.SubtypeName);

            
            mTermControls.Add(checkboxControl);
            mTermControls.Add(checkboxControlLabel);


            SeperatorControl2 =
                MyAPIGateway.TerminalControls.CreateControl<IMyTerminalControlSeparator, IMyConveyorSorter>(
                    "Pacas00.Seperator2");
            SeperatorControl2.Visible = (b) => Definitions.CargoTeleportTethered.Contains(b.BlockDefinition.SubtypeName);
            mTermControls.Add(SeperatorControl2);



            nfcControlLabel =
                MyAPIGateway.TerminalControls.CreateControl<IMyTerminalControlLabel, IMyConveyorSorter>("Pacas00.nfcControlLabel");

            nfcControlLabel.Label = MyStringId.GetOrCompute("NFC Settings (Optional)");
            nfcControlLabel.Visible = (b) => Definitions.CargoTeleportTethered.Contains(b.BlockDefinition.SubtypeName);

            mTermControls.Add(nfcControlLabel);

            

            nfcPlateNameControl =
                MyAPIGateway.TerminalControls.CreateControl<IMyTerminalControlTextbox, IMyConveyorSorter>(
                    "Pacas00.nfcPlateNameControl");
            nfcPlateNameControl.Title = MyStringId.GetOrCompute("NFC Plate Name");
            nfcPlateNameControl.Getter =
                (b) => new System.Text.StringBuilder(b.GameLogic.GetAs<CargoTeleporterLogic>().nfcPlateName);
            nfcPlateNameControl.Setter = (b, v) => setNFCPlateName(b, v.ToString());
            nfcPlateNameControl.Visible = (b) => Definitions.CargoTeleportTethered.Contains(b.BlockDefinition.SubtypeName);
            mTermControls.Add(nfcPlateNameControl);

            nfcRangeControl =
                MyAPIGateway.TerminalControls.CreateControl<IMyTerminalControlSlider, IMyConveyorSorter>(
                    "Pacas00.nfcRangeControl");


            nfcRangeControl.Title = MyStringId.GetOrCompute("NFC Plate Range");
            nfcRangeControl.SetLimits(5f, 25f);
            nfcRangeControl.Getter = (b) => b.GameLogic.GetAs<CargoTeleporterLogic>().nfcRange;
            nfcRangeControl.Setter = (b, v) => setNFCRange(b, v);
            nfcRangeControl.Tooltip = MyStringId.GetOrCompute("");
            nfcRangeControl.Visible = (b) => Definitions.CargoTeleportTethered.Contains(b.BlockDefinition.SubtypeName);
            mTermControls.Add(nfcRangeControl);

            foreach (var control in mTermControls)
            {
                MyAPIGateway.TerminalControls.AddControl<IMyConveyorSorter>(control);
            }
        }

        public void setNFCPlateName(IMyTerminalBlock b, string v)
        {
            b.GameLogic.GetAs<CargoTeleporterLogic>().nfcPlateName = v;
            UpdateNameStorage(b);
            RedrawGUI(b);
        }

        public void setNFCRange(IMyTerminalBlock b, float v)
        {
            b.GameLogic.GetAs<CargoTeleporterLogic>().nfcRange = v;
            UpdateNameStorage(b);
            RedrawGUI(b);
        }

        public void ToggleMode(IMyTerminalBlock b)
        {
            bool state = b.GameLogic.GetAs<CargoTeleporterLogic>().toFromMode;
            setBoolMode(b, !state);
            UpdateNameStorage(b);
            RedrawGUI(b);
        }

        public void RedrawGUI(IMyTerminalBlock b)
        {
            SeperatorControl.RedrawControl();
            targetNameControl.RedrawControl();
            checkboxControlLabel.RedrawControl();
            checkboxControl.RedrawControl();
            SeperatorControl2.RedrawControl();
            nfcControlLabel.RedrawControl();
            nfcPlateNameControl.RedrawControl();
            nfcRangeControl.Tooltip = MyStringId.GetOrCompute(b.GameLogic.GetAs<CargoTeleporterLogic>().nfcRange.ToString());
            nfcRangeControl.UpdateVisual();
            nfcRangeControl.RedrawControl();
            
        }

        public void setBoolMode(IMyTerminalBlock b, bool value)
        {

            b.GameLogic.GetAs<CargoTeleporterLogic>().toFromMode = value;
            UpdateNameStorage(b);
            RedrawGUI(b);
        }

        public void setTargetName(IMyTerminalBlock b, string value)
        {
            b.GameLogic.GetAs<CargoTeleporterLogic>().targetName = value;
            UpdateNameStorage(b);
            RedrawGUI(b);
        }

        public bool toFromMode = false;
        public string targetName = "";

        public string nfcPlateName = "";
        public float nfcRange = 10f;

        public void LoadFromNameStorage()
        {
            if (CargoTeleporter == null || CargoTeleporter.DisplayNameText == null)
            {
                Entity.NeedsUpdate |= MyEntityUpdateEnum.BEFORE_NEXT_FRAME;
            }
            else
            {
                string tmp = "";
                if (CargoTeleporter.DisplayNameText.Contains("<T:"))
                {
                    int start = CargoTeleporter.DisplayNameText.IndexOf("<T:") + 3;
                    int end = CargoTeleporter.DisplayNameText.IndexOf(">", start);
                    tmp = CargoTeleporter.DisplayNameText.Substring(start, end - start);
                    targetName = tmp;
                    toFromMode = false;

                }
                else if (CargoTeleporter.DisplayNameText.Contains("<F:"))
                {
                    int start = CargoTeleporter.DisplayNameText.IndexOf("<F:") + 3;
                    int end = CargoTeleporter.DisplayNameText.IndexOf(">", start);
                    tmp = CargoTeleporter.DisplayNameText.Substring(start, end - start);
                    targetName = tmp;
                    toFromMode = true;
                }
                else
                {
                    targetName = tmp;
                    toFromMode = false;
                }

                string PlateName = "";

                if (CargoTeleporter.DisplayNameText.Contains("<NFC:"))
                {
                    int start = CargoTeleporter.DisplayNameText.IndexOf("<NFC:") + 5;
                    int end = CargoTeleporter.DisplayNameText.IndexOf(">", start);
                    PlateName = CargoTeleporter.DisplayNameText.Substring(start, end - start);
                }
                float range = 10;
                if (CargoTeleporter.DisplayNameText.Contains("<Range:"))
                {
                    int start = CargoTeleporter.DisplayNameText.IndexOf("<Range:") + 7;
                    int end = CargoTeleporter.DisplayNameText.IndexOf(">", start);
                    try
                    {
                        range = Convert.ToSingle(CargoTeleporter.DisplayNameText.Substring(start, end - start));
                    }
                    catch (Exception ex)
                    {
                        Logging.WriteException(ex);
                        Logging.WriteLine("Trying to recover _" + CargoTeleporter.DisplayNameText.Substring(start, end - start));
                        try
                        {
                            string value = CargoTeleporter.DisplayNameText.Substring(start, end - start);
                            if (value == null || value.Length < 1)
                            {
                                //What
                            }
                            else
                            {
                                int val = Convert.ToInt32(CargoTeleporter.DisplayNameText.Substring(start, end - start));
                                range = val;
                            }
                        }
                        catch (Exception ex2)
                        {
                            Logging.WriteLine("Failed to recover _" + CargoTeleporter.DisplayNameText.Substring(start, end - start));
                            Logging.WriteException(ex);
                        }
                    }
                }
                if (range > 25) range = 25;

                nfcPlateName = PlateName;
                nfcRange = range;


                LoadedData = true;
            }

        }


        public void UpdateNameStorage(IMyTerminalBlock b)
        {
            bool mode = b.GameLogic.GetAs<CargoTeleporterLogic>().toFromMode;
            string target = b.GameLogic.GetAs<CargoTeleporterLogic>().targetName;

            string NfcPlate = b.GameLogic.GetAs<CargoTeleporterLogic>().nfcPlateName;
            float NfcRange = b.GameLogic.GetAs<CargoTeleporterLogic>().nfcRange;

            //Update Name
            string newName = "";
            try
            {
                if (b != null && b.DisplayNameText != null)
                {
                    if (target.Length > 0)
                    {

                        string tmp = "";
                        if (b.DisplayNameText.Contains("<T:"))
                        {
                            int start = b.DisplayNameText.IndexOf("<T:") + 3;
                            int end = b.DisplayNameText.IndexOf(">", start);
                            tmp = b.DisplayNameText.Substring(start, end - start);

                            newName = b.DisplayNameText.Replace("<T:" + tmp + ">", "<T:" + target + ">");
                            if (mode)
                            {
                                newName = b.DisplayNameText.Replace("<T:" + tmp + ">", "<F:" + target + ">");
                            }


                        }
                        else if (b.DisplayNameText.Contains("<F:"))
                        {
                            int start = b.DisplayNameText.IndexOf("<F:") + 3;
                            int end = b.DisplayNameText.IndexOf(">", start);
                            tmp = b.DisplayNameText.Substring(start, end - start);

                            newName = b.DisplayNameText.Replace("<F:" + tmp + ">", "<F:" + target + ">");
                            if (!mode)
                            {
                                newName = b.DisplayNameText.Replace("<F:" + tmp + ">", "<T:" + target + ">");
                            }

                        }
                        else
                        {

                            {
                                if (mode) newName = (b.DisplayNameText + " <F:" + target + ">");
                                else newName = (b.DisplayNameText + " <T:" + target + ">");
                            }
                        }
                    }
                    else
                    {
                        //Remove the field entirely
                        string tmp = "";
                        if (b.DisplayNameText.Contains("<T:"))
                        {
                            int start = b.DisplayNameText.IndexOf("<T:") + 3;
                            int end = b.DisplayNameText.IndexOf(">", start);
                            tmp = b.DisplayNameText.Substring(start, end - start);

                            newName = b.DisplayNameText.Replace(" <T:" + tmp + ">", "");
                        }
                        else if (b.DisplayNameText.Contains("<F:"))
                        {
                            int start = b.DisplayNameText.IndexOf("<F:") + 3;
                            int end = b.DisplayNameText.IndexOf(">", start);
                            tmp = b.DisplayNameText.Substring(start, end - start);

                            newName = b.DisplayNameText.Replace(" <F:" + tmp + ">", "");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.WriteException(ex);
            }


            try
            {
                string PlateName = "";
                if (newName.Contains("<NFC:"))
                {
                    if (NfcPlate.Length < 1)
                    {
                        int start = newName.IndexOf("<NFC:") + 5;
                        int end = newName.IndexOf(">", start);
                        PlateName = newName.Substring(start, end - start);
                        newName = newName.Replace(" <NFC:" + PlateName + ">", "");
                    }
                    else
                    {
                        int start = newName.IndexOf("<NFC:") + 5;
                        int end = newName.IndexOf(">", start);
                        PlateName = newName.Substring(start, end - start);
                        newName = newName.Replace("<NFC:" + PlateName + ">", "<NFC:" + NfcPlate + ">");
                    }

                }
                else
                {
                    if (NfcPlate.Length > 0)
                    {
                        newName = (newName + " <NFC:" + NfcPlate + ">");
                    }
                }

            }
            catch (Exception ex)
            {
                Logging.WriteException(ex);
            }


            try
            {
                string Range = "";
                if (newName.Contains("<Range:"))
                {
                    if (NfcPlate.Length < 1)
                    {
                        int start = newName.IndexOf("<Range:") + 7;
                        int end = newName.IndexOf(">", start);
                        Range = newName.Substring(start, end - start);
                        newName = newName.Replace(" <Range:" + Range + ">", "");
                    }
                    else
                    {
                        int start = newName.IndexOf("<Range:") + 7;
                        int end = newName.IndexOf(">", start);
                        Range = newName.Substring(start, end - start);
                        newName = newName.Replace("<Range:" + Range + ">", "<Range:" + NfcRange + ">");
                    }
                }
                else
                {
                    if (NfcPlate.Length > 0)
                    {
                        newName = (newName + " <Range:" + NfcRange + ">");
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.WriteException(ex);
            }

            try
            {
                (b as IMyTerminalBlock).SetCustomName(newName);
            }
            catch (Exception ex)
            {
                Logging.WriteException(ex);
            }
        }

        public override void UpdateAfterSimulation()
        {
            base.UpdateAfterSimulation();
        }
        public override void UpdateAfterSimulation10()
        {
            base.UpdateAfterSimulation10();
        }
        public override void UpdateAfterSimulation100()
        {
            base.UpdateAfterSimulation100();
        }
        public override void UpdateBeforeSimulation()
        {
            base.UpdateBeforeSimulation();
        }
        public override void UpdateBeforeSimulation10()
        {
            base.UpdateBeforeSimulation10();
        }
        public override void UpdateOnceBeforeFrame()
        {
            if (!LoadedData) LoadFromNameStorage();

            AddTerminalControls();
            base.UpdateOnceBeforeFrame();
        }
        public override void UpdatingStopped()
        {
            base.UpdatingStopped();
        }

        public override void UpdateBeforeSimulation100()
        {
            LogicAbort = false;
            base.UpdateBeforeSimulation100();
            if (CargoTeleporter == null) return;
            try
            {
                if (!(CargoTeleporter as IMyFunctionalBlock).Enabled)
                {
                    //if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine(CargoTeleporter.DisplayNameText + " is powered off");
                    return;
                }
                else
                {
                    if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine(CargoTeleporter.DisplayNameText + " is powered on");
                }
            }
            catch (Exception ex)
            {
                Logging.WriteLine(ex.Message);
            }
            if (LogicAbort) return;
            try
            {
                if (MyAPIGateway.Session == null)
                {
                    if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("MyAPIGateway.Session is null");
                    return;
                }

                if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("MainRun");

                if (Settings.Instance.settings.Debug_Tethered && true)
                {
                    Logging.WriteLine("Status Check");
                    if (CargoTeleporter == null) Logging.WriteLine("CargoTeleporter is NULL");
                    if (CargoTeleporter.DisplayNameText == null) Logging.WriteLine("CargoTeleporter.DisplayNameText is NULL");
                    
                    if (terminal == null) Logging.WriteLine("terminal is NULL");
                    if (Entity == null) Logging.WriteLine("Entity is NULL");
                    if (myEnt == null) Logging.WriteLine("myEnt is NULL");
                    if (inventory == null) Logging.WriteLine("inventory is NULL");
                }

                if (inventory == null)
                {
                    if (myEnt != null && myEnt.HasInventory)
                    {
                        inventory = myEnt.GetInventoryBase() as MyInventory;
                    }
                }

                if (CargoTeleporter.DisplayNameText.Contains("<NFC:"))
                {
                    NFCPlateMode();
                }
                else
                {
                    LaserAntennaMode();
                }
            }
            catch (Exception ex)
            {
                Logging.WriteLine(ex.Message);
            }
        }

        private void NFCPlateMode()
        {
            if (Definitions.CargoTeleportTethered.Contains(CargoTeleporter.BlockDefinition.SubtypeId))
            {
                if (inventory.Empty()) return;
                //long playerId = CargoTeleporter.OwnerId;

                string PlateName = "";

                if (CargoTeleporter.DisplayNameText.Contains("<NFC:"))
                {
                    int start = CargoTeleporter.DisplayNameText.IndexOf("<NFC:") + 5;
                    int end = CargoTeleporter.DisplayNameText.IndexOf(">", start);
                    PlateName = CargoTeleporter.DisplayNameText.Substring(start, end - start);
                }
                double range = 10;
                if (CargoTeleporter.DisplayNameText.Contains("<Range:"))
                {
                    int start = CargoTeleporter.DisplayNameText.IndexOf("<Range:") + 7;
                    int end = CargoTeleporter.DisplayNameText.IndexOf(">", start);
                    range = Convert.ToDouble(CargoTeleporter.DisplayNameText.Substring(start, end - start));
                }

                range = range + 0.1;

                if (range > 25.1) range = 25.1;

                if (PlateName.Length < 2) return;

                IMyVirtualMass NFCPlate = null;

                if (Settings.Instance.settings.Debug_NFC) Logging.WriteLine("GetBlocks");

                List<IMySlimBlock> Blocks = new List<IMySlimBlock>();

                List<IMySlimBlock> slimAnts = new List<IMySlimBlock>();
                CargoTeleporter.CubeGrid.GetBlocks(slimAnts, x => x is IMySlimBlock);
                foreach (IMySlimBlock block in slimAnts)
                {
                    if (block.FatBlock != null)
                    {
                        Blocks.Add(block);
                    }
                }

                IMyEntity targetEntNFCPlate = null;
                targetEntNFCPlate = Blocks.Where(x => x.FatBlock != null && ValidNFCPlate(x.FatBlock.BlockDefinition.SubtypeId)).First().FatBlock;
                if (targetEntNFCPlate == null) Logging.WriteLine("targetEnt null");

                if (Settings.Instance.settings.Debug_NFC) Logging.WriteLine("IMyRadioAntenna");
                NFCPlate = targetEntNFCPlate as IMyVirtualMass;

                if (Settings.Instance.settings.Debug_NFC) Logging.WriteLine("VRageMath");

                VRageMath.BoundingSphereD sphere = new VRageMath.BoundingSphereD(NFCPlate.GetPosition(), range);

                if (Settings.Instance.settings.Debug_NFC) Logging.WriteLine("VRageMath2");

                if (Settings.Instance.settings.Debug_NFC) Logging.WriteLine("" + CargoTeleporter.OwnerId);
                try
                {
                    if (Settings.Instance.settings.Debug_NFC) Logging.WriteLine("Getting ents in box");
                    List<IMyEntity> NFC2 = MyAPIGateway.Entities.GetEntitiesInSphere(ref sphere);
                    if (Settings.Instance.settings.Debug_NFC)
                        foreach (IMyEntity e in NFC2)
                        {
                            Logging.WriteLine("" + e.ToString() + " " + e.GetType().ToString());
                        }

                    List<IMyEntity> NFC = MyAPIGateway.Entities.GetEntitiesInSphere(ref sphere).Where(x => x is IMyCubeBlock && ValidNFCPlate((x as IMyCubeBlock).BlockDefinition.SubtypeId) && x.EntityId != NFCPlate.EntityId).ToList();
                    if (NFC.Count <= 0)
                    {
                        if (Settings.Instance.settings.Debug_NFC) Logging.WriteLine("There was Nothing");
                        return;
                    }

                    List<IMySlimBlock> entities = new List<IMySlimBlock>();

                    (NFC.First() as IMyCubeBlock).CubeGrid.GetBlocks(entities);

                    if (Settings.Instance.settings.Debug_NFC) Logging.WriteLine("Entitys");
                    HashSet<IMyCubeBlock> gridBlocks = new HashSet<IMyCubeBlock>();
                    foreach (IMySlimBlock slim in entities)
                    {
                        gridBlocks.Add(slim.FatBlock);
                    }
                    if (Settings.Instance.settings.Debug_NFC) Logging.WriteLine("PostGrids");
                    if (Settings.Instance.settings.Debug_NFC) Logging.WriteLine("entities " + entities.Count);
                    if (Settings.Instance.settings.Debug_NFC) Logging.WriteLine("gridBlocks " + gridBlocks.Count);
                    DateTime startBlockLoop = DateTime.Now;

                    if (!CargoTeleporter.DisplayNameText.Contains("-Off-"))
                    {
                        string name = "";

                        DateTime compStart = DateTime.Now;
                        bool toMode = true;
                        if (CargoTeleporter.DisplayNameText.Contains("<T:"))
                        {
                            int start = CargoTeleporter.DisplayNameText.IndexOf("<T:") + 3;
                            int end = CargoTeleporter.DisplayNameText.IndexOf(">", start);
                            name = CargoTeleporter.DisplayNameText.Substring(start, end - start);
                        }
                        else if (CargoTeleporter.DisplayNameText.Contains("<F:"))
                        {
                            int start = CargoTeleporter.DisplayNameText.IndexOf("<F:") + 3;
                            int end = CargoTeleporter.DisplayNameText.IndexOf(">", start);
                            name = CargoTeleporter.DisplayNameText.Substring(start, end - start);
                            toMode = false;
                        }


                        if (name.Length > 2)
                        {
                            if (Settings.Instance.settings.Debug_NFC) Logging.WriteLine("PostName " + name);
                            IMyEntity targetEnt = null;

                            targetEnt = gridBlocks.Where(x => x != null && x.DisplayNameText != null && x.DisplayNameText == name && OwnershipUtils.isSameFactionOrOwner(CargoTeleporter, x)).First();
                            if (targetEnt == null) Logging.WriteLine("targetEnt null");
                            if (targetEnt is IMyCubeBlock)
                            {
                                IMyCubeBlock targetCube = (IMyCubeBlock)targetEnt;

                                IMyInventory inventoryTO = null;

                                MyEntity entity = targetEnt as MyEntity;
                                if (entity.HasInventory)
                                {
                                    inventoryTO = entity.GetInventoryBase() as MyInventory;
                                }

                                int slotCount = 0;
                                if (toMode) slotCount = inventory.GetItems().Count; else slotCount = inventoryTO.GetItems().Count;
                                if (slotCount > 1)
                                {
                                    for (int i = 0; i < slotCount; i++)
                                    {
                                        if (!inventoryTO.IsFull && !inventory.Empty() && toMode && ValidItemType(inventory.GetItems()[i]))
                                        {
                                            inventory.TransferItemTo(inventoryTO, 0, null, true, inventory.GetItems()[i].Amount, false);
                                        }
                                        else if (!inventoryTO.Empty() && !inventory.IsFull && !toMode && ValidItemType(inventoryTO.GetItems()[i]))
                                        {
                                            inventory.TransferItemFrom(inventoryTO, 0, null, true, inventoryTO.GetItems()[i].Amount, false);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!inventoryTO.IsFull && !inventory.Empty() && toMode && ValidItemType(inventory.GetItems()[0]))
                                    {
                                        inventory.TransferItemTo(inventoryTO, 0, null, true, inventory.GetItems()[0].Amount, false);
                                    }
                                    else if (!inventoryTO.Empty() && !inventory.IsFull && !toMode && ValidItemType(inventoryTO.GetItems()[0]))
                                    {
                                        inventory.TransferItemFrom(inventoryTO, 0, null, true, inventoryTO.GetItems()[0].Amount, false);
                                    }
                                }
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    Logging.WriteLine(ex.Message);
                }
            }
        }

        private void LaserAntennaMode()
        {
            if (Definitions.CargoTeleportTethered.Contains(CargoTeleporter.BlockDefinition.SubtypeId))
            {
                //long playerId = CargoTeleporter.OwnerId;
                string name = "";
                string GridName = string.Empty;
                DateTime compStart = DateTime.Now;
                bool toMode = true;
                if (LogicAbort) return;
                if (CargoTeleporter.DisplayNameText.Contains("<G:"))
                {
                    int start = CargoTeleporter.DisplayNameText.IndexOf("<G:") + 3;
                    int end = CargoTeleporter.DisplayNameText.IndexOf(">", start);
                    GridName = CargoTeleporter.DisplayNameText.Substring(start, end - start);
                }

                if (CargoTeleporter.DisplayNameText.Contains("<T:"))
                {
                    int start = CargoTeleporter.DisplayNameText.IndexOf("<T:") + 3;
                    int end = CargoTeleporter.DisplayNameText.IndexOf(">", start);
                    name = CargoTeleporter.DisplayNameText.Substring(start, end - start);
                }
                else if (CargoTeleporter.DisplayNameText.Contains("<F:"))
                {
                    int start = CargoTeleporter.DisplayNameText.IndexOf("<F:") + 3;
                    int end = CargoTeleporter.DisplayNameText.IndexOf(">", start);
                    name = CargoTeleporter.DisplayNameText.Substring(start, end - start);
                    toMode = false;
                }
                if (inventory.Empty() && toMode) return;
                if (LogicAbort) return;

                //Find Connected Antennas and their grids.
                if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("GetBlocks");

                List<IMySlimBlock> slimAnts = new List<IMySlimBlock>();
                CargoTeleporter.CubeGrid.GetBlocks(slimAnts, x => x.FatBlock != null);
                if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("slimAnts: " + slimAnts.Count);


                List<IMySlimBlock> targetEntAnts = new List<IMySlimBlock>();
                if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("Lists");
                targetEntAnts.AddRange(
                slimAnts.Where(x => x.FatBlock != null && x.FatBlock is IMyLaserAntenna &&
                Definitions.TetherUplink500.Contains(((x.FatBlock as IMyLaserAntenna).BlockDefinition.SubtypeId))
                ));
                if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("1");
                targetEntAnts.AddRange(slimAnts.Where(x => x.FatBlock != null && x.FatBlock is IMyLaserAntenna &&
                Definitions.TetherUplink1500.Contains(((x.FatBlock as IMyLaserAntenna).BlockDefinition.SubtypeId))
                ));
                if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("2");
                targetEntAnts.AddRange(slimAnts.Where(x => x.FatBlock != null && x.FatBlock is IMyLaserAntenna &&
                Definitions.TetherUplinkCustom.Contains(((x.FatBlock as IMyLaserAntenna).BlockDefinition.SubtypeId))
                ));
                if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("3");
                if (LogicAbort) return;

                if (targetEntAnts.Count == 0) Logging.WriteLine("targetEntAnts empty");

                List<IMyCubeGrid> CubeGrids = new List<IMyCubeGrid>();


                foreach (IMySlimBlock blk in targetEntAnts)
                {
                    try
                    {
                        if (LogicAbort) return;
                        IMyLaserAntenna ant = blk.FatBlock as IMyLaserAntenna;
                        if (Settings.Instance.settings.Debug_Tethered)
                        {
                            string[] l = ant.DetailedInfo.Split('\n');
                            Logging.WriteLine("L " + l.Length);
                            Logging.WriteLine("L " + l[2].Length);
                            Logging.WriteLine("L " + l[2][2]);
                        }
                        if (commonCodeFragments.IsConnected(ant.DetailedInfo))
                        {
                            //Its Connected
                            VRageMath.BoundingSphereD sphere = new VRageMath.BoundingSphereD(ant.TargetCoords, 5.0);
                            IMyLaserAntenna antTarget = null;
                            if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("4 _" + MyAPIGateway.Entities.GetEntitiesInSphere(ref sphere).Count);
                            if (GridName == string.Empty)
                            {
                                antTarget = MyAPIGateway.Entities.GetEntitiesInSphere(ref sphere)
                                .Where(x => x is IMyLaserAntenna &&
                                ((sphere.Contains((x as IMyLaserAntenna).GetPosition())) == VRageMath.ContainmentType.Contains) &&
                                commonCodeFragments.IsConnected((x as IMyLaserAntenna).DetailedInfo)

                                ).First() as IMyLaserAntenna;
                                if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("GridName " + ant.CubeGrid.DisplayName);
                            }
                            else
                            {
                                antTarget = MyAPIGateway.Entities.GetEntitiesInSphere(ref sphere)
                                .Where(x => x is IMyLaserAntenna &&
                                ((sphere.Contains((x as IMyLaserAntenna).GetPosition())) == VRageMath.ContainmentType.Contains) &&
                                commonCodeFragments.IsConnected((x as IMyLaserAntenna).DetailedInfo) && (x as IMyTerminalBlock).CubeGrid.DisplayName == GridName

                                ).First() as IMyLaserAntenna;
                                if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("GridName_ " + ant.CubeGrid.DisplayName);
                            }

                            if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("5");

                            if (!CubeGrids.Contains(ant.CubeGrid)) CubeGrids.Add(antTarget.CubeGrid as IMyCubeGrid);

                            if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("6");
                        }
                    }
                    catch (Exception ex)
                    {
                        Logging.WriteLine(ex.Message);
                        Logging.WriteLine(ex.StackTrace);
                        try
                        {
                            Logging.WriteLine(ex.TargetSite.ToString());
                        }
                        catch
                        {
                        }
                    }
                }
                if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("AllLasers resulted in Grids = " + CubeGrids.Count);
                if (Settings.Instance.settings.Debug_Tethered)
                    if (CubeGrids.Count == 0)
                    {
                        foreach (IMySlimBlock b in targetEntAnts)
                        {
                            Logging.WriteLine(b.FatBlock.BlockDefinition.SubtypeId);
                        }
                    }
                if (LogicAbort) return;
                try
                {
                    List<IMySlimBlock> entities = new List<IMySlimBlock>();

                    foreach (IMyCubeGrid grid in CubeGrids)
                    {
                        grid.GetBlocks(entities, x => x is IMySlimBlock && x.FatBlock != null);
                    }
                    if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("entities postgrid " + entities.Count);
                    if (LogicAbort) return;
                    HashSet<IMyCubeBlock> gridBlocks = new HashSet<IMyCubeBlock>();
                    foreach (IMySlimBlock slim in entities)
                    {
                        gridBlocks.Add(slim.FatBlock);
                    }

                    if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("gridBlocks " + gridBlocks.Count);
                    DateTime startBlockLoop = DateTime.Now;

                    //if (!CargoTeleporter.DisplayNameText.Contains("-Off-"))
                    {
                        if (LogicAbort) return;

                        if (name.Length > 2)
                        {
                            if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("PostName " + name);
                            IMyEntity targetEnt = null;

                            if (Settings.Instance.settings.StrictMode)
                            {
                                targetEnt = gridBlocks.Where(x => x != null && x.DisplayNameText != null && x.DisplayNameText == name && OwnershipUtils.isSameFactionOrOwner(CargoTeleporter, x) && ValidSubtype(x.BlockDefinition.SubtypeId)).First();
                            }
                            else
                            {
                                targetEnt = gridBlocks.Where(x => x != null && x.DisplayNameText != null && x.DisplayNameText == name && OwnershipUtils.isSameFactionOrOwner(CargoTeleporter, x)).First();
                            }

                            if (targetEnt == null) Logging.WriteLine("targetEnt null");
                            if (targetEnt is IMyCubeBlock)
                            {
                                IMyCubeBlock targetCube = (IMyCubeBlock)targetEnt;
                                IMyInventory inventoryTO = null;
                                MyEntity entity = targetEnt as MyEntity;
                                if (entity.HasInventory)
                                {
                                    inventoryTO = entity.GetInventoryBase() as MyInventory;
                                }

                                int slotCount = 0;
                                if (toMode) slotCount = inventory.GetItems().Count; else slotCount = inventoryTO.GetItems().Count;
                                if (slotCount > 1)
                                {
                                    for (int i = 0; i < slotCount; i++)
                                    {
                                        if (!inventoryTO.IsFull && !inventory.Empty() && toMode && ValidItemType(inventory.GetItems()[i]))
                                        {
                                            inventory.TransferItemTo(inventoryTO, 0, null, true, inventory.GetItems()[i].Amount, false);
                                        }
                                        else if (!inventoryTO.Empty() && !inventory.IsFull && !toMode && ValidItemType(inventoryTO.GetItems()[i]))
                                        {
                                            inventory.TransferItemFrom(inventoryTO, 0, null, true, inventoryTO.GetItems()[i].Amount, false);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!inventoryTO.IsFull && !inventory.Empty() && toMode && ValidItemType(inventory.GetItems()[0]))
                                    {
                                        inventory.TransferItemTo(inventoryTO, 0, null, true, inventory.GetItems()[0].Amount, false);
                                    }
                                    else if (!inventoryTO.Empty() && !inventory.IsFull && !toMode && ValidItemType(inventoryTO.GetItems()[0]))
                                    {
                                        inventory.TransferItemFrom(inventoryTO, 0, null, true, inventoryTO.GetItems()[0].Amount, false);
                                    }
                                }
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    Logging.WriteLine(ex.Message);
                }
            }
        }

        public bool ValidSubtype(string subtypeId)
        {
            if (Definitions.CargoTeleportTethered.Contains(subtypeId))
                return true;
            else return false;
        }

        private bool ValidNFCPlate(string subtypeId)
        {
            if (Definitions.NFCPad.Contains(subtypeId)) return true;
            return false;
        }

        private bool ValidItemType(VRage.Game.ModAPI.Ingame.IMyInventoryItem myInventoryItem)
        {

            if (Settings.Instance.settings.AmmoAllowed && myInventoryItem.Content.TypeId.ToString()
                == "MyObjectBuilder_AmmoMagazine") return true;
            if (Settings.Instance.settings.ComponentsAllowed && myInventoryItem.Content.TypeId.ToString()
                == "MyObjectBuilder_Component") return true;
            if (Settings.Instance.settings.IngotsAllowed && myInventoryItem.Content.TypeId.ToString()
                == "MyObjectBuilder_Ingot") return true;
            if (Settings.Instance.settings.OresAllowed && myInventoryItem.Content.TypeId.ToString()
                == "MyObjectBuilder_Ore") return true;
            if (Settings.Instance.settings.WeaponsToolsAllowed && myInventoryItem.Content.TypeId.ToString()
                == "MyObjectBuilder_PhysicalGunObject") return true;

            return false;
        }
    }
}
