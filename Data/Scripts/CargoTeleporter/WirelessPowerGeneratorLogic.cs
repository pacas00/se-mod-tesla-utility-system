﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sandbox.Common.ObjectBuilders;
using Sandbox.Common.ObjectBuilders.Definitions;
using Sandbox.Definitions;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.EntityComponents;
using Sandbox.Game.Lights;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Weapons;
using SpaceEngineers.Game.ModAPI;
using System.Timers;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.Entity;
using VRage.Game.ModAPI;
using VRage.Game.ModAPI.Interfaces;
using VRage.Game.ObjectBuilders;
using VRage.ObjectBuilders;
using VRage.Library.Utils;
using VRage.ModAPI;
using VRage.Utils;

using VRage.Game;
using VRage.Game.ObjectBuilders;
using VRage.Game.ObjectBuilders.Definitions;

namespace CargoTeleporter
{
    [MyEntityComponentDescriptor(typeof(MyObjectBuilder_Reactor), new string[] { "SmallBlockPowerGenerator" })]
    public class WirelessPowerGeneratorLogic : MyGameLogicComponent
    {
        MyObjectBuilder_EntityBase ObjectBuilder;
        IMyReactor Reactor = null;
        public float currentOutputFloat = 1.0f;
        MyEntity myEnt = null;
        IMyInventory inventory = null;

        public override void Init(MyObjectBuilder_EntityBase objectBuilder)
        {
            Entity.NeedsUpdate |= MyEntityUpdateEnum.EACH_100TH_FRAME;
            ObjectBuilder = objectBuilder;
            Reactor = Entity as IMyReactor;
            myEnt = Entity as MyEntity;
            if (myEnt.HasInventory)
            {
                inventory = myEnt.GetInventoryBase() as MyInventory;
            }
            base.Init(objectBuilder);
        }

        public override MyObjectBuilder_EntityBase GetObjectBuilder(bool copy = false)
        {
            return copy ? ObjectBuilder.Clone() as MyObjectBuilder_EntityBase : ObjectBuilder;
        }

        public override void Close()
        {
            base.Close();
        }

        public override void UpdateAfterSimulation()
        {
            base.UpdateAfterSimulation();
        }
        public override void UpdateAfterSimulation10()
        {
            base.UpdateAfterSimulation10();
        }
        public override void UpdateAfterSimulation100()
        {
            base.UpdateAfterSimulation100();
        }
        public override void UpdateBeforeSimulation()
        {
            base.UpdateBeforeSimulation();
        }
        public override void UpdateBeforeSimulation10()
        {
            base.UpdateBeforeSimulation10();
        }
        public override void UpdateOnceBeforeFrame()
        {
            base.UpdateOnceBeforeFrame();
        }
        public override void UpdatingStopped()
        {
            base.UpdatingStopped();
        }

        public override void UpdateBeforeSimulation100()
        {
            base.UpdateBeforeSimulation100();
            MainLoop();
        }

        public void MainLoop()
        {
            if (Reactor == null) return;
            try
            {
                currentOutputFloat = Reactor.Components.Get<MyResourceSourceComponent>().CurrentOutputByType(new MyDefinitionId(typeof(MyObjectBuilder_GasProperties), "Electricity"));
            }
            catch { }

            try
            {
                if (!(Reactor as IMyFunctionalBlock).Enabled)
                {
                    if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine(Reactor.DisplayNameText + " is powered off");
                    return;
                }
                else
                {
                    if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine(Reactor.DisplayNameText + " is powered on");
                }
            }
            catch (Exception ex)
            {
                Logging.WriteLine(ex.Message);
            }
            try
            {
                if (MyAPIGateway.Session == null)
                {
                    if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("MyAPIGateway.Session is null");
                    return;
                }

                if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("MainRun");


                //Here

                //Find Connected Antennas and their grids.
                if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("GetBlocks");

                List<IMySlimBlock> slimAnts = new List<IMySlimBlock>();
                (Reactor as IMyCubeBlock).CubeGrid.GetBlocks(slimAnts, x => x.FatBlock != null);
                if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("slimAnts: " + slimAnts.Count);


                List<IMySlimBlock> targetEntAnts = new List<IMySlimBlock>();
                if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("Lists");
                targetEntAnts.AddRange(
                slimAnts.Where(x => x.FatBlock != null && x.FatBlock is IMyLaserAntenna &&
                Definitions.PowerReceiverUplink500.Contains(((x.FatBlock as IMyLaserAntenna).BlockDefinition.SubtypeId))
                ));
                if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("1");

                if (targetEntAnts.Count == 0) Logging.WriteLine("targetEntAnts empty - PWG");
                if (targetEntAnts.Count == 0) return;

                bool foundConnected = false;
                foreach (IMySlimBlock blk in targetEntAnts)
                {
                    IMyLaserAntenna ant = blk.FatBlock as IMyLaserAntenna;
                    if (commonCodeFragments.IsConnected(ant.DetailedInfo))
                        foundConnected = true;
                }

                if (foundConnected == false)
                {
                    //Trash reactor contents

                    IMyInventory inventory = null;
                    if (myEnt.HasInventory)
                    {
                        inventory = myEnt.GetInventoryBase() as MyInventory;
                    }
                    if (inventory == null)
                    {
                        throw new Exception("NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS CAKE NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH");
                    }

                    if (!inventory.Empty())
                    {
                        if (inventory.GetItems().Count > 1)
                        {
                            for (int i = inventory.GetItems().Count -1; i > -1; i--)
                            {
                                inventory.RemoveItemsAt(i);
                            }
                        }
                        else
                        {
                            inventory.RemoveItemsAt(0);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Logging.WriteLine(ex.Message);
            }

        }

    }
}
