﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sandbox.Common.ObjectBuilders;
using Sandbox.Common.ObjectBuilders.Definitions;
using Sandbox.Definitions;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.EntityComponents;
using Sandbox.Game.Lights;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Weapons;
using SpaceEngineers.Game.ModAPI;
using System.Timers;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.Entity;
using VRage.Game.ModAPI;
using VRage.Game.ModAPI.Interfaces;
using VRage.Game.ObjectBuilders;
using VRage.ObjectBuilders;
using VRage.Library.Utils;
using VRage.ModAPI;
using VRage.Utils;

namespace CargoTeleporter
{
    [MyEntityComponentDescriptor(typeof(MyObjectBuilder_LaserAntenna), new string[] { "SmallBlockPowerReceiverUplink500" })]
    public class PowerRecieverUplinkLogic500 : MyGameLogicComponent
    {
        MyObjectBuilder_EntityBase ObjectBuilder;
        IMyLaserAntenna LaserAntenna = null;
        public override void Init(MyObjectBuilder_EntityBase objectBuilder)
        {
            Entity.NeedsUpdate |= MyEntityUpdateEnum.EACH_100TH_FRAME;
            ObjectBuilder = objectBuilder;
            LaserAntenna = Entity as IMyLaserAntenna;
            base.Init(objectBuilder);
        }

        public override MyObjectBuilder_EntityBase GetObjectBuilder(bool copy = false)
        {
            return copy ? ObjectBuilder.Clone() as MyObjectBuilder_EntityBase : ObjectBuilder;
        }

        public override void Close()
        {
            base.Close();
        }

        public override void UpdateAfterSimulation()
        {
            base.UpdateAfterSimulation();
        }
        public override void UpdateAfterSimulation10()
        {
            base.UpdateAfterSimulation10();
        }
        public override void UpdateAfterSimulation100()
        {
            base.UpdateAfterSimulation100();
        }
        public override void UpdateBeforeSimulation()
        {
            base.UpdateBeforeSimulation();
        }
        public override void UpdateBeforeSimulation10()
        {
            base.UpdateBeforeSimulation10();
        }
        public override void UpdateOnceBeforeFrame()
        {
            base.UpdateOnceBeforeFrame();
        }
        public override void UpdatingStopped()
        {
            base.UpdatingStopped();
        }

        public override void UpdateBeforeSimulation100()
        {
            base.UpdateBeforeSimulation100();
            MainLoop();
        }

        public void MainLoop()
        {

            if (

            LaserAntenna == null) return;
            try
            {
                if (!(LaserAntenna as IMyFunctionalBlock).Enabled)
                {
                    if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine(LaserAntenna.DisplayNameText + " is powered off");
                    return;
                }
                else
                {
                    if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine(LaserAntenna.DisplayNameText + " is powered on");
                }
            }
            catch (Exception ex)
            {
                Logging.WriteLine(ex.Message);
            }
            try
            {
                if (MyAPIGateway.Session == null)
                {
                    if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("MyAPIGateway.Session is null");
                    return;
                }

                if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("MainRun");
                if (Definitions.PowerReceiverUplink500.Contains(LaserAntenna.BlockDefinition.SubtypeId))
                {
                    string name = "";
                    DateTime compStart = DateTime.Now;
                    bool foundConnected = false;
                    IMyLaserAntenna antTarget = null;
                    //Find Connected Antennas and their grids.
                    if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("CheckInfo");

                    if (commonCodeFragments.IsConnected(LaserAntenna.DetailedInfo))
                    {
                        if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("Connected");
                        //Its Connected
                        VRageMath.BoundingSphereD sphere = new VRageMath.BoundingSphereD(LaserAntenna.TargetCoords, 5.0);

                        antTarget = MyAPIGateway.Entities.GetEntitiesInSphere(ref sphere)
                            .Where(x => x is IMyLaserAntenna &&
                            ((sphere.Contains((x as IMyLaserAntenna).GetPosition())) == VRageMath.ContainmentType.Contains)
                            ).First() as IMyLaserAntenna;

                        if (Definitions.PowerSenderUplink500.Contains((antTarget as IMyLaserAntenna).BlockDefinition.SubtypeId))
                        {
                            if (commonCodeFragments.IsConnected((antTarget as IMyLaserAntenna).DetailedInfo))
                            {
                                if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("Found Valid");
                                foundConnected = true;
                            }
                            else
                                foundConnected = false;
                        }
                        else
                        {
                            if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("Found Invalid");
                            LaserAntenna.SetTargetCoords("GPS::0:0:0:");
                            LaserAntenna.GetActionWithName("OnOff_Off").Apply(LaserAntenna as IMyCubeBlock);

                            return;
                        }
                    }

                    //Here

                    //Find Connected Antennas and their grids.
                    if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("GetBlocks");

                    List<IMySlimBlock> slimReactors = new List<IMySlimBlock>();
                    (LaserAntenna as IMyCubeBlock).CubeGrid.GetBlocks(slimReactors, x => x.FatBlock != null);
                    if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("slimAnts: " + slimReactors.Count);


                    List<IMySlimBlock> targetEntReactors = new List<IMySlimBlock>();
                    if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("Lists");
                    targetEntReactors.AddRange(
                    slimReactors.Where(x => x.FatBlock != null && x.FatBlock is IMyReactor &&
                    Definitions.PowerGenerator.Contains(((x.FatBlock as IMyReactor).BlockDefinition.SubtypeId))
                    ));

                    if (targetEntReactors.Count == 0) Logging.WriteLine("targetEntReactors empty");
                    if (targetEntReactors.Count == 0) return;

                    if (foundConnected)
                    {
                        if (targetEntReactors.Count == 1)
                        {
                            IMySlimBlock slmBlk = targetEntReactors.First();
                            IMyReactor Reactor = slmBlk.FatBlock as IMyReactor;
                            try
                            {
                                WirelessPowerGeneratorLogic react = (WirelessPowerGeneratorLogic)Reactor.GameLogic;
                                PowerSenderUplinkLogic500 transmitter = (PowerSenderUplinkLogic500)antTarget.GameLogic;
                                if (Settings.Instance.settings.Debug_WirelessPower)
                                {
                                    Logging.WriteLine(Reactor.DisplayNameText + " " + react.currentOutputFloat.ToString());
                                    Logging.WriteLine(LaserAntenna.DisplayNameText + " <- " + antTarget.DisplayNameText + " " + transmitter.desiredInput);
                                }

                                        transmitter.setInputRequired(react.currentOutputFloat);

                            }
                            catch (Exception ex)
                            {
                                Logging.WriteException(ex);
                            }

                            MyEntity myEnt = slmBlk.FatBlock as MyEntity;
                            IMyInventory inventory = null;
                            if (myEnt.HasInventory)
                            {
                                inventory = myEnt.GetInventory() as MyInventory;
                            }
                            if (inventory == null)
                            {
                                throw new Exception("NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS CAKE NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH");
                            }



                            //inventory.RemoveItemsAt(0, null, true, false);
                            MyObjectBuilder_PhysicalObject itemTemplete = new MyObjectBuilder_Component() { SubtypeName = Definitions.WirelessEnergy.SubtypeID };

                            int amount = 250 * 1000000;
                            int amountTippingPoint = 125 * 1000000;

                            if (inventory.Empty())
                            {
                                //empty add all
                                inventory.AddItems(250, itemTemplete);
                            }
                            else
                            {
                                List<VRage.Game.ModAPI.Ingame.IMyInventoryItem> items = inventory.GetItems();
                                foreach (VRage.Game.ModAPI.Ingame.IMyInventoryItem item in items)
                                {
                                    if (item.Content.SubtypeId.String == Definitions.WirelessEnergy.SubtypeID)
                                    {
                                        amount = amount - Convert.ToInt32(item.Amount.RawValue);
                                    }
                                }
                                if (amount > 0 && amount > amountTippingPoint)
                                {
                                    inventory.AddItems(amount / 1000000 + 1, itemTemplete);
                                }
                            }
                        }
                        else
                        {
                            //Work out where we are in the chain
                            List<IMySlimBlock> slimAnts = new List<IMySlimBlock>();
                            (LaserAntenna as IMyCubeBlock).CubeGrid.GetBlocks(slimAnts, x => x.FatBlock != null);
                            if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("MultislimAnts: " + slimAnts.Count);

                            List<IMySlimBlock> targetEntAnts = new List<IMySlimBlock>();
                            if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("MultiLists");
                            targetEntAnts.AddRange(
                            slimAnts.Where(x => x.FatBlock != null && x.FatBlock is IMyLaserAntenna &&
                            Definitions.PowerReceiverUplink500.Contains(((x.FatBlock as IMyLaserAntenna).BlockDefinition.SubtypeId))
                            ));
                            if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("Multi1");

                            if (targetEntAnts.Count == 0) Logging.WriteLine("MultitargetEntAnts empty");
                            if (targetEntAnts.Count == 0) return;

                            int pos = 0;

                            for (int i = 0; i < targetEntAnts.Count; i++)
                            {
                                if (targetEntAnts[i].FatBlock.EntityId == LaserAntenna.EntityId)
                                {
                                    pos = i;
                                    break;
                                }
                            }

                            if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("GetReactor " + pos);
                            IMyReactor Reactor = targetEntReactors[pos].FatBlock as IMyReactor;
                            try
                            {
                                WirelessPowerGeneratorLogic react = (WirelessPowerGeneratorLogic)Reactor.GameLogic;
                                PowerSenderUplinkLogic500 transmitter = (PowerSenderUplinkLogic500)antTarget.GameLogic;
                                if (Settings.Instance.settings.Debug_WirelessPower)
                                {
                                    Logging.WriteLine(Reactor.DisplayNameText + " " + react.currentOutputFloat.ToString());
                                    Logging.WriteLine(LaserAntenna.DisplayNameText + " <- " + antTarget.DisplayNameText + " " + transmitter.desiredInput);
                                }
                                if (transmitter != null && react != null)
                                {
                                    try
                                    {
                                        transmitter.setInputRequired(react.currentOutputFloat);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logging.WriteException(ex);
                                        Logging.WriteLine("Ignoring Previous Error");
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Logging.WriteException(ex);
                            }

                            MyEntity myEnt = targetEntReactors[pos].FatBlock as MyEntity;
                            
                            IMyInventory inventory = null;
                            if (myEnt.HasInventory)
                            {
                                inventory = myEnt.GetInventory();
                            }
                            if (inventory == null)
                            {
                                throw new Exception("NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS CAKE NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH NULL INVENTORY MEANS DEATH");
                            }


                            //inventory.RemoveItemsAt(0, null, true, false);
                            MyObjectBuilder_PhysicalObject itemTemplete = new MyObjectBuilder_Component() { SubtypeName = Definitions.WirelessEnergy.SubtypeID };

                            int amount = 250 * 1000000;
                            int amountTippingPoint = 125 * 1000000;
                            
                            if (inventory.Empty())
                            {
                                //empty add all
                                inventory.AddItems(250, itemTemplete);
                            }
                            else
                            {
                                List<VRage.Game.ModAPI.Ingame.IMyInventoryItem> items = inventory.GetItems();
                                foreach (VRage.Game.ModAPI.Ingame.IMyInventoryItem item in items)
                                {
                                    if (item.Content.SubtypeId.String == Definitions.WirelessEnergy.SubtypeID)
                                    {
                                        amount = amount - Convert.ToInt32(item.Amount.RawValue);
                                    }
                                }
                                if (amount > 0 && amount > amountTippingPoint)
                                {
                                    inventory.AddItems(amount / 1000000 + 1, itemTemplete);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.WriteLine(ex.Message);
            }

        }



    }
}
