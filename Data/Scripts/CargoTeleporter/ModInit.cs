﻿using Sandbox.Common;
using Sandbox.Definitions;
using Sandbox.ModAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRage.Game.Components;
using VRage.Game.ModAPI;

namespace CargoTeleporter
{
    [MySessionComponentDescriptor(MyUpdateOrder.BeforeSimulation)]
    public class ModInit : MySessionComponentBase
    {
        // Credit to Digi for his Server-Side config file that he shared on the KeenSWH forums. 
        // http://forum.keenswh.com/threads/snippet-server-side-configuration-file.7289796/

        protected bool InitializedMod;

        protected bool Initialize()
        {
            Logging.WriteLine("Mod Starting Up.");
            if (MyDefinitionManager.Static.Loading)
            {
                Logging.WriteLine("MUST WAIT FOR DEFS TO LOAD!");
                return false;
            }

            try
            {
                if (MyAPIGateway.Session == null)
                    return false;

                if (!Logging.LoggingReady) Logging.InitialiseLogging();
                if (!Logging.LoggingReady) return false;
                Logging.WriteLine("Logging Initialised!");

                if (Settings.Instance == null)
                    Settings.Initialise();

                if (Settings.Instance != null && Settings.Instance.loaded == true)
                { }
                else { return false; }
            }
            catch
            {
                return false;
            }

            return true;
        } //Return true for success, false to try again on next update


        protected void Shutdown() //Called from UnloadData()
        {
            if (!MyAPIGateway.Multiplayer.MultiplayerActive)
            { //SP
                Settings.Instance.Save();
            }
            else
            { //MP
                MyAPIGateway.Multiplayer.UnregisterMessageHandler(PACKET_SEND_SETTINGS_TO_CLIENT, ReceivedSettings);

                if (MyAPIGateway.Multiplayer.IsServer)
                {
                    MyAPIGateway.Multiplayer.UnregisterMessageHandler(PACKET_REQUEST_SETTINGS_FROM_SERVER, ReceivedSettingsRequest);
                    Settings.Instance.Save();
                } else
                {
                    MyAPIGateway.Multiplayer.UnregisterMessageHandler(PACKET_SEND_SETTINGS_TO_CLIENT, ReceivedSettings);
                }
            }

            Logging.ShutdownLogging();
        }

        //27 / 12 / 5  (2015)
        private const ushort PACKET_SEND_SETTINGS_TO_CLIENT = 27125;
        private const ushort PACKET_REQUEST_SETTINGS_FROM_SERVER = PACKET_SEND_SETTINGS_TO_CLIENT + 1;
        private static Encoding encode = Encoding.ASCII;

        protected void PostInitLoadStageOne() //Only called after we are initialized
        {
            if (!MyAPIGateway.Multiplayer.MultiplayerActive)
            {
                //SP
                Logging.WriteLine("Single Player Mode");
                PostInitLoadStageTwo();
            }
            else //MP
            {
                Logging.WriteLine("Multi Player Mode");

                // everybody can receive settings
                // MyAPIGateway.Multiplayer.RegisterMessageHandler(PACKET_SEND_SETTINGS_TO_CLIENT, ReceivedSettings);

                if (MyAPIGateway.Multiplayer.IsServer)
                {
                    // only the server can receive setting requests
                    MyAPIGateway.Multiplayer.RegisterMessageHandler(PACKET_REQUEST_SETTINGS_FROM_SERVER, ReceivedSettingsRequest);
                    PostInitLoadStageTwo();
                    Logging.WriteLine("Server ready to send settings.");
                }
                else
                {
                    // Only want the client to receive settings,
                    MyAPIGateway.Multiplayer.RegisterMessageHandler(PACKET_SEND_SETTINGS_TO_CLIENT, ReceivedSettings);

                    // When you're joining as a player this will ask the server for the settings
                    Logging.WriteLine("Requesting Settings");
                    byte[] bytes = encode.GetBytes(MyAPIGateway.Multiplayer.MyId.ToString());
                    MyAPIGateway.Multiplayer.SendMessageToServer(PACKET_REQUEST_SETTINGS_FROM_SERVER, bytes, true);
                }

            }

        }

        private static void PostInitLoadStageTwo()
        {
            Definitions.pruneDefs();
            Definitions.adjustDefs();
        }

        public void Update()
        {
            if (!InitializedMod)
            {
                InitializedMod = Initialize();
                if (InitializedMod) PostInitLoadStageOne();
            }
            if (InitializedMod)
            {
                //Stuff here
            }
        }

        public override void LoadData()
        {
            if (InitializedMod)
                PostInitLoadStageOne();
            base.LoadData();
        }

        protected override void UnloadData()
        {
            Shutdown();
            base.UnloadData();
        }

        public override void UpdateBeforeSimulation()
        {
            Update();
            base.UpdateBeforeSimulation();
        }

        public override void Simulate()
        {
            Update();
            base.Simulate();
        }
        public override void UpdateAfterSimulation()
        {
            Update();
            base.UpdateAfterSimulation();
        }


        /*
         * This method sends the settings to a specified player or to the server
         */
        public static void SendSettings(ulong sendTo)
        {
            Logging.WriteLine("Sending settings to " + sendTo);
            byte[] bytes = encode.GetBytes(Settings.Instance.settingstext);

            if (sendTo == 0)
                MyAPIGateway.Multiplayer.SendMessageToServer(PACKET_SEND_SETTINGS_TO_CLIENT, bytes, true);
            else
                MyAPIGateway.Multiplayer.SendMessageTo(PACKET_SEND_SETTINGS_TO_CLIENT, bytes, sendTo, true);
        }

        /*
         * Method triggered when a player changes a setting and sends it to the server using SendSettings(0).
         */
        protected static void ReceivedSettings(byte[] bytes)
        {
            Logging.WriteLine("Received Settings");
            string data = encode.GetString(bytes);
            string[] lines = data.Split('\n');

            foreach (string line in lines)
            {
                Settings.Instance.parseSettingsLine(line.Trim());
            }

            // Synchronize settings with everybody else if server
            if (MyAPIGateway.Multiplayer.IsServer)
            {
                List<IMyPlayer> players = new List<IMyPlayer>();
                MyAPIGateway.Multiplayer.Players.GetPlayers(players);

                foreach (IMyPlayer player in players)
                {
                    SendSettings((ulong)player.PlayerID);
                }
            }
            else // if client, finish init
            {
                PostInitLoadStageTwo();
            }
        }

        /*
         * Triggered when the server gets a request to send settings to a player
         */
        protected static void ReceivedSettingsRequest(byte[] bytes)
        {
            string data = encode.GetString(bytes);
            ulong sendTo;

            if (!ulong.TryParse(data, out sendTo))
            {
                Logging.WriteLine("Unable to convert '" + data + "' to ulong!");
                return;
            }
            SendSettings(sendTo);
        }
    }
}
