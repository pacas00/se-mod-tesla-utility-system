﻿using Sandbox.Definitions;
using Sandbox.ModAPI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CargoTeleporter
{
    class Settings
    {
        private static Settings instance = null;
        
        public static Settings Instance
        {
            get
            {
                if (MyAPIGateway.Utilities == null)
                    return null;

                if (instance == null)
                {
                    instance = new Settings();
                    instance.Load();
                }

                if (!instance.loaded) instance.Load();
                    

                return instance;
            }
        }

        //Static Above

        //Instance Below

        public bool loaded = false;
        public SettingsObject settings = new SettingsObject();
        public string settingstext = "";

        public void Load()
        {
            if (MyAPIGateway.Utilities == null)
                return;

            loaded = true;
            if (MyAPIGateway.Utilities.FileExistsInLocalStorage("Settings.txt", typeof(Settings)))
            try
            {
                TextReader reader = MyAPIGateway.Utilities.ReadFileInLocalStorage("Settings.txt", typeof(Settings));
                {
                    bool hasLine = true;
                    try
                    {                        
                        while (hasLine)
                        {
                            string line = reader.ReadLine();
                            if (line == null) {
                                hasLine = false;
                                break;
                            }
                                settingstext += (line + "\n");
                                parseSettingsLine(line);

                        }
                    } catch (Exception ex)
                    {
                        Logging.WriteLine("Settings: " + ex.Message);
                        hasLine = false;
                        reader.Close();
                    }
                        reader.Close();
                    }
            }
            catch (Exception ex)
            {
                Logging.WriteLine(String.Format("Load(): {0}", ex.ToString()));
            }
            Logging.WriteLine("Settings Loaded!");
        }

        public void parseSettingsLine(string line)
        {
            if (line.Trim().StartsWith("#")) return;
            if (line.Trim().Length < 3) return;
            string[] parts = line.Split('=');

            switch (parts[0])
            {
                case ("AmmoAllowed"):
                    settings.AmmoAllowed = Convert.ToBoolean(parts[1]);
                    break;

                case ("ComponentsAllowed"):
                    settings.ComponentsAllowed = Convert.ToBoolean(parts[1]);
                    break;

                case ("IngotsAllowed"):
                    settings.IngotsAllowed = Convert.ToBoolean(parts[1]);
                    break;

                case ("OresAllowed"):
                    settings.OresAllowed = Convert.ToBoolean(parts[1]);
                    break;

                case ("WeaponsToolsAllowed"):
                    settings.WeaponsToolsAllowed = Convert.ToBoolean(parts[1]);
                    break;

                case ("StrictMode"):
                    settings.StrictMode = Convert.ToBoolean(parts[1]);
                    break;

                case ("Debug_NFC"):
                    settings.Debug_NFC = Convert.ToBoolean(parts[1]);
                    break;

                case ("Debug_Tethered"):
                    settings.Debug_Tethered = Convert.ToBoolean(parts[1]);
                    break;

                case ("Debug_WirelessPower"):
                    settings.Debug_WirelessPower = Convert.ToBoolean(parts[1]);
                    break;

                case ("Enable_WirelessPower"):
                    settings.Enable_WirelessPower = Convert.ToBoolean(parts[1]);
                    break;

                case ("Enable_CargoTeleporting"):
                    settings.Enable_CargoTeleporting = Convert.ToBoolean(parts[1]);
                    break;

                case ("WirelessPowerRangeSmall"):
                    settings.WirelessPowerRangeSmall = Convert.ToSingle(parts[1]);
                    break;

                case ("WirelessPowerMWSmall"):
                    settings.WirelessPowerMWSmall = Convert.ToSingle(parts[1]);
                    break;

                case ("PrototypeFeaturesEnabled"):
                    settings.PrototypeFeaturesEnabled = Convert.ToBoolean(parts[1]);
                    break;

                case ("Enable_CustomTetherCargo"):
                    settings.Enable_CustomTetherCargo = Convert.ToBoolean(parts[1]);
                    break;

                case ("Enable_CustomTetherPower"):
                    settings.Enable_CustomTetherPower = Convert.ToBoolean(parts[1]);
                    break;

                case ("CustomTetherCargoRange"):
                    settings.CustomTetherCargoRange = Convert.ToSingle(parts[1]);
                    break;

                case ("CustomTetherPowerMW"):
                    settings.CustomTetherPowerMW = Convert.ToSingle(parts[1]);
                    break;

                case ("CustomTetherPowerRange"):
                    settings.CustomTetherPowerRange = Convert.ToSingle(parts[1]);
                    break;
                    
                case ("CustomTetherPowerCostMulti"):
                    settings.CustomTetherPowerCostMulti = Convert.ToDouble(parts[1]);
                    break;
                    
                case ("CustomTetherCargoCostMulti"):
                    settings.CustomTetherCargoCostMulti = Convert.ToDouble(parts[1]);
                    break;

                case ("WirelessPowerCompCostMulti"):
                    settings.WirelessPowerCompCostMulti = Convert.ToDouble(parts[1]);
                    break;

                case ("CargoTetherCompCostMulti"):
                    settings.CargoTetherCompCostMulti = Convert.ToDouble(parts[1]);
                    break;

                default: break;
            }
        }

        internal static void Initialise()
        {
            if (instance == null)
            {
                instance = new Settings();
                instance.Load();
            }
        }

        public void Save()
        {
            if (MyAPIGateway.Utilities == null)
                return;

            MyAPIGateway.Utilities.DeleteFileInLocalStorage("Settings.txt", typeof(Settings));

            TextWriter writer = MyAPIGateway.Utilities.WriteFileInLocalStorage("Settings.txt", typeof(Settings));
            
            try
            {
                
                {
                    writer.WriteLine("");
                    writer.WriteLine("# -------------------------------");
                    writer.WriteLine("# - Allowed Item Teleport Types -");
                    writer.WriteLine("# -------------------------------");
                    writer.WriteLine("");
                    writer.WriteLine("#AmmoAllowed - Is Ammo allowed to be teleported");
                    writer.WriteLine("AmmoAllowed" + "=" + settings.AmmoAllowed);
                    writer.WriteLine("");
                    writer.WriteLine("#ComponentsAllowed - Are Components allowed to be teleported");
                    writer.WriteLine("ComponentsAllowed" + "=" + settings.ComponentsAllowed);
                    writer.WriteLine("");
                    writer.WriteLine("#IngotsAllowed - ComponentsAllowed ammo allowed to be teleported");
                    writer.WriteLine("IngotsAllowed" + "=" + settings.IngotsAllowed);
                    writer.WriteLine("");
                    writer.WriteLine("#OresAllowed - Are Ores allowed to be teleported");
                    writer.WriteLine("OresAllowed" + "=" + settings.OresAllowed);
                    writer.WriteLine("");
                    writer.WriteLine("#WeaponsToolsAllowed - Are Weapons and Tools allowed to be teleported");
                    writer.WriteLine("WeaponsToolsAllowed" + "=" + settings.WeaponsToolsAllowed);
                    writer.WriteLine("");


                    writer.WriteLine("");
                    writer.WriteLine("# --------------------");
                    writer.WriteLine("# - General Settings -");
                    writer.WriteLine("# --------------------");
                    writer.WriteLine("");
                    writer.WriteLine("#Enable_WirelessPower - Are the Wireless Power Blocks enabled?");
                    writer.WriteLine("Enable_WirelessPower" + "=" + settings.Enable_WirelessPower);
                    writer.WriteLine("");
                    writer.WriteLine("#Enable_CargoTeleporting - Are the Tethered Cargo Teleporter Blocks enabled?");
                    writer.WriteLine("Enable_CargoTeleporting" + "=" + settings.Enable_CargoTeleporting);
                    writer.WriteLine("");
                    writer.WriteLine("");
                    writer.WriteLine("#StrictMode - Can we only teleport to another Translocator. Set to false for any block.");
                    writer.WriteLine("StrictMode" + "=" + settings.StrictMode);
                    writer.WriteLine("");
                    

                    writer.WriteLine("");
                    writer.WriteLine("# ----------------------------------------");
                    writer.WriteLine("# - Small Wireless Power Tether Settings -");
                    writer.WriteLine("# ----------------------------------------");
                    writer.WriteLine("");
                    writer.WriteLine("#WirelessPowerRangeSmall - Range for Large to Small Wireless Power tethers. Default 500   ( 500 m )");
                    writer.WriteLine("WirelessPowerRangeSmall" + "=" + settings.WirelessPowerRangeSmall);
                    writer.WriteLine("");
                    writer.WriteLine("#WirelessPowerMWSmall - Desired MW for Large to Small Wireless Power Geneator. Default 6   ( 6MW )");
                    writer.WriteLine("WirelessPowerMWSmall" + "=" + settings.WirelessPowerMWSmall);
                    writer.WriteLine("");
                    

                    writer.WriteLine("");
                    writer.WriteLine("# ---------------------=--------");
                    writer.WriteLine("# - Component Cost Multipliers -");
                    writer.WriteLine("# ------------------------------");
                    writer.WriteLine("");
                    writer.WriteLine("#WirelessPowerCompCostMulti - Cost Multiplier for the Large to Small Wireless Power tethers. Default 1.0");
                    writer.WriteLine("WirelessPowerCompCostMulti" + "=" + settings.WirelessPowerCompCostMulti);
                    writer.WriteLine("");
                    writer.WriteLine("#CargoTetherCompCostMulti - Cost Multiplier for the Large Custom Tethered Cargo tethers. Default 1.0");
                    writer.WriteLine("CargoTetherCompCostMulti" + "=" + settings.CargoTetherCompCostMulti);
                    writer.WriteLine("");


                    writer.WriteLine("");
                    writer.WriteLine("# -------------------");
                    writer.WriteLine("# - Debug Settings");
                    writer.WriteLine("# -------------------");
                    writer.WriteLine("");
                    writer.WriteLine("#Debug_WirelessPower - Is Debug Logging on for Wireless Power Blocks");
                    writer.WriteLine("Debug_WirelessPower" + "=" + settings.Debug_WirelessPower);
                    writer.WriteLine("");
                    writer.WriteLine("#Debug_NFC - Is Debug Logging on for NFC Transposer Blocks");
                    writer.WriteLine("Debug_NFC" + "=" + settings.Debug_NFC);
                    writer.WriteLine("");
                    writer.WriteLine("#Debug_Tethered - Is Debug Logging on for Tethered Cargo Teleporter Blocks");
                    writer.WriteLine("Debug_Tethered" + "=" + settings.Debug_Tethered);
                    writer.WriteLine("");

                    writer.WriteLine("");
                    writer.WriteLine("# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    writer.WriteLine("#    Prototype Features are disabled by default. Enable at your own risk  ");
                    writer.WriteLine("# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    writer.WriteLine("PrototypeFeaturesEnabled" + "=" + settings.PrototypeFeaturesEnabled);
                    writer.WriteLine("");

                    if (settings.PrototypeFeaturesEnabled)
                    {                        
                        writer.WriteLine("");
                        writer.WriteLine("#Enable_CustomTetherPower - Are the Custom Large Only Wireless Power Blocks enabled?");
                        writer.WriteLine("Enable_CustomTetherPower" + "=" + settings.Enable_CustomTetherPower);
                        writer.WriteLine("");
                        writer.WriteLine("#CustomTetherPowerRange - Range for Large Only Wireless Power tethers. Default 100000   ( 100,000 m )");
                        writer.WriteLine("CustomTetherPowerRange" + "=" + settings.CustomTetherPowerRange);
                        writer.WriteLine("");
                        writer.WriteLine("#CustomTetherPowerMW - Desired MW for Large Only Wireless Power Geneator. Default 250   ( 250MW )");
                        writer.WriteLine("CustomTetherPowerMW" + "=" + settings.CustomTetherPowerMW);
                        writer.WriteLine("");
                        writer.WriteLine("#CustomTetherPowerCostMulti - Cost Multiplier for the Large Only Wireless Power tethers. Default 1.0");
                        writer.WriteLine("CustomTetherPowerCostMulti" + "=" + settings.CustomTetherPowerCostMulti);
                        writer.WriteLine("");
                        writer.WriteLine("");
                        writer.WriteLine("#Enable_CustomTetherCargo - Are the Custom Tethered Cargo Teleporter Blocks enabled?");
                        writer.WriteLine("Enable_CustomTetherCargo" + "=" + settings.Enable_CustomTetherCargo);
                        writer.WriteLine("");
                        writer.WriteLine("#CustomTetherCargoRange - Range for the Large Custom Tethered Cargo tethers. Default 100000   ( 100,000 m )");
                        writer.WriteLine("CustomTetherCargoRange" + "=" + settings.CustomTetherCargoRange);
                        writer.WriteLine("");
                        writer.WriteLine("#CustomTetherCargoCostMulti - Cost Multiplier for the Large Custom Tethered Cargo tethers. Default 1.0");
                        writer.WriteLine("CustomTetherCargoCostMulti" + "=" + settings.CustomTetherCargoCostMulti);
                        writer.WriteLine("");
                    }

                }
                writer.Flush();
                writer.Close();
            }
            catch (Exception ex)
            {
                Logging.WriteLine(String.Format("Save(): {0}", ex.ToString()));
                writer.Flush();
                writer.Close();
            }
            Logging.WriteLine("Settings Saved!");
        }

    }


    public class SettingsObject
    {
        public bool PrototypeFeaturesEnabled = false;


        public bool AmmoAllowed = true;

        public bool ComponentsAllowed = true;

        public bool IngotsAllowed = true;

        public bool OresAllowed = true;

        public bool WeaponsToolsAllowed = true;

        public bool StrictMode = true;





        public bool Debug_WirelessPower = false;
        public bool Debug_NFC = false;
        public bool Debug_Tethered = false;



        public bool Enable_WirelessPower = true;
        public bool Enable_CargoTeleporting = true;

        public float WirelessPowerRangeSmall = 500f;
        public float WirelessPowerMWSmall = 6f;



        public bool Enable_CustomTetherCargo = false;
        public bool Enable_CustomTetherPower = false;


        public float CustomTetherCargoRange = 100000f;
        public double CustomTetherCargoCostMulti = 1.0;

        public float CustomTetherPowerMW = 250f;
        public float CustomTetherPowerRange = 100000f;
        public double CustomTetherPowerCostMulti = 1.0;

        public double WirelessPowerCompCostMulti = 1.0;
        public double CargoTetherCompCostMulti = 1.0;
    }


}
