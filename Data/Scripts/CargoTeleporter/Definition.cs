﻿using Sandbox.Common.ObjectBuilders;
using Sandbox.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRage.ObjectBuilders;
using VRage.Game.ModAPI;
using VRage.Game;
using VRage.Game.ObjectBuilders;

namespace CargoTeleporter
{
    static class Definitions
    {
        //methods

        //This is the part where we remove what we dont want
        public static void pruneDefs()
        {
            if (!Settings.Instance.settings.Enable_WirelessPower)
            {
                foreach (string subtype in PowerSenderUplink500)
                {
                    MyDefinitionId def = new MyDefinitionId(typeof(MyObjectBuilder_LaserAntenna), subtype);
                    MyCubeBlockDefinition CBDef = MyDefinitionManager.Static.GetCubeBlockDefinition(def);
                    CBDef.Public = false;
                }

                foreach (string subtype in PowerReceiverUplink500)
                {
                    MyDefinitionId def = new MyDefinitionId(typeof(MyObjectBuilder_LaserAntenna), subtype);
                    MyCubeBlockDefinition CBDef = MyDefinitionManager.Static.GetCubeBlockDefinition(def);
                    CBDef.Public = false;
                }

                foreach (string subtype in PowerGenerator)
                {
                    MyDefinitionId def = new MyDefinitionId(typeof(MyObjectBuilder_Reactor), subtype);
                    MyCubeBlockDefinition CBDef = MyDefinitionManager.Static.GetCubeBlockDefinition(def);
                    CBDef.Public = false;
                }                
            }

            if (!Settings.Instance.settings.Enable_CargoTeleporting)
            {
                foreach (string subtype in TetherUplink500)
                {
                    MyDefinitionId def = new MyDefinitionId(typeof(MyObjectBuilder_LaserAntenna), subtype);
                    MyCubeBlockDefinition CBDef = MyDefinitionManager.Static.GetCubeBlockDefinition(def);
                    CBDef.Public = false;
                }

                foreach (string subtype in TetherUplink1500)
                {
                    MyDefinitionId def = new MyDefinitionId(typeof(MyObjectBuilder_LaserAntenna), subtype);
                    MyCubeBlockDefinition CBDef = MyDefinitionManager.Static.GetCubeBlockDefinition(def);
                    CBDef.Public = false;
                }

                foreach (string subtype in NFCPad)
                {
                    MyDefinitionId def = new MyDefinitionId(typeof(MyObjectBuilder_VirtualMass), subtype);
                    MyCubeBlockDefinition CBDef = MyDefinitionManager.Static.GetCubeBlockDefinition(def);
                    CBDef.Public = false;
                }

                foreach (string subtype in CargoTeleportTethered)
                {
                    MyDefinitionId def = new MyDefinitionId(typeof(MyObjectBuilder_ConveyorSorter), subtype);
                    MyCubeBlockDefinition CBDef = MyDefinitionManager.Static.GetCubeBlockDefinition(def);
                    CBDef.Public = false;
                }
            }

            if (Settings.Instance.settings.Enable_CustomTetherCargo && Settings.Instance.settings.Enable_CargoTeleporting)
            {
                foreach (string subtype in TetherUplinkCustom)
                {
                    MyDefinitionId def = new MyDefinitionId(typeof(MyObjectBuilder_LaserAntenna), subtype);
                    MyCubeBlockDefinition CBDef = MyDefinitionManager.Static.GetCubeBlockDefinition(def);
                    CBDef.Public = true;
                }

                double compMultiRange = (int)((Settings.Instance.settings.CustomTetherCargoRange / 100000f));
                if (compMultiRange < 1) compMultiRange = 1;
                compMultiRange = compMultiRange * Settings.Instance.settings.CustomTetherCargoCostMulti;
                float range = Settings.Instance.settings.CustomTetherCargoRange;
                foreach (string subtype in TetherUplinkCustom)
                {
                    MyDefinitionId def = new MyDefinitionId(typeof(MyObjectBuilder_LaserAntenna), subtype);
                    MyLaserAntennaDefinition CBDef = MyDefinitionManager.Static.GetCubeBlockDefinition(def) as MyLaserAntennaDefinition;
                    CBDef.MaxRange = range;
                    CBDef.DisplayNameString = String.Format("Large Tether Uplink ({0}m)", range);
                    CBDef.Public = true;

                    for (int i = 0; i < CBDef.Components.Length; i++)
                    {
                        CBDef.Components[i].Count = (int) (CBDef.Components[i].Count * compMultiRange);
                    }
                }
            }

            if (Settings.Instance.settings.Enable_CustomTetherPower && Settings.Instance.settings.Enable_WirelessPower)
            {
                foreach (string subtype in PowerSenderUplinkCustom)
                {
                    MyDefinitionId def = new MyDefinitionId(typeof(MyObjectBuilder_LaserAntenna), subtype);
                    MyCubeBlockDefinition CBDef = MyDefinitionManager.Static.GetCubeBlockDefinition(def);
                    CBDef.Public = true;
                }

                foreach (string subtype in PowerReceiverUplinkCustom)
                {
                    MyDefinitionId def = new MyDefinitionId(typeof(MyObjectBuilder_LaserAntenna), subtype);
                    MyCubeBlockDefinition CBDef = MyDefinitionManager.Static.GetCubeBlockDefinition(def);
                    CBDef.Public = true;
                }

                foreach (string subtype in PowerGeneratorCustom)
                {
                    MyDefinitionId def = new MyDefinitionId(typeof(MyObjectBuilder_Reactor), subtype);
                    MyCubeBlockDefinition CBDef = MyDefinitionManager.Static.GetCubeBlockDefinition(def);
                    CBDef.Public = true;
                }


                double compMultiRange = (int)((Settings.Instance.settings.CustomTetherPowerRange / 100000f));
                if (compMultiRange < 1) compMultiRange = 1;
                compMultiRange = compMultiRange * Settings.Instance.settings.CustomTetherPowerCostMulti;
                float range = Settings.Instance.settings.CustomTetherPowerRange;
                foreach (string subtype in PowerSenderUplinkCustom)
                {
                    MyDefinitionId def = new MyDefinitionId(typeof(MyObjectBuilder_LaserAntenna), subtype);
                    MyLaserAntennaDefinition CBDef = MyDefinitionManager.Static.GetCubeBlockDefinition(def) as MyLaserAntennaDefinition;
                    CBDef.MaxRange = range;
                    CBDef.DisplayNameString = String.Format("Large Wireless Power Transmitter ({0}m)", range);

                    for (int i = 0; i < CBDef.Components.Length; i++)
                    {
                        CBDef.Components[i].Count = (int)(CBDef.Components[i].Count * compMultiRange);
                    }
                }

                foreach (string subtype in PowerReceiverUplinkCustom)
                {
                    MyDefinitionId def = new MyDefinitionId(typeof(MyObjectBuilder_LaserAntenna), subtype);
                    MyLaserAntennaDefinition CBDef = MyDefinitionManager.Static.GetCubeBlockDefinition(def) as MyLaserAntennaDefinition;
                    CBDef.MaxRange = range;
                    CBDef.DisplayNameString = String.Format("Large Wireless Power Receiver ({0}m)", range);

                    for (int i = 0; i < CBDef.Components.Length; i++)
                    {
                        CBDef.Components[i].Count = (int)(CBDef.Components[i].Count * compMultiRange);
                    }
                }


                double compMultiPower = (int)((Settings.Instance.settings.CustomTetherPowerMW / 250f));
                if (compMultiPower < 1) compMultiPower = 1;
                compMultiRange = compMultiRange * Settings.Instance.settings.CustomTetherPowerCostMulti;
                float powerGen = Settings.Instance.settings.CustomTetherPowerMW;
                float powerDraw = powerGen + (powerGen / 10);

                foreach (string subtype in PowerSenderUplinkCustom)
                {
                    MyDefinitionId def = new MyDefinitionId(typeof(MyObjectBuilder_LaserAntenna), subtype);
                    MyLaserAntennaDefinition CBDef = MyDefinitionManager.Static.GetCubeBlockDefinition(def) as MyLaserAntennaDefinition;
                    CBDef.PowerInputLasing = powerDraw;
                }

                foreach (string subtype in PowerGeneratorCustom)
                {
                    MyDefinitionId def = new MyDefinitionId(typeof(MyObjectBuilder_Reactor), subtype);
                    MyReactorDefinition CBDef = MyDefinitionManager.Static.GetCubeBlockDefinition(def) as MyReactorDefinition;
                    CBDef.MaxPowerOutput = powerGen;

                    for (int i = 0; i < CBDef.Components.Length; i++)
                    {
                        CBDef.Components[i].Count = (int)(CBDef.Components[i].Count * compMultiPower);
                    }
                }


            }

        }

        public static void adjustDefs() {
            if (Settings.Instance.settings.Enable_WirelessPower)
            if (Settings.Instance.settings.WirelessPowerRangeSmall != 500f || Settings.Instance.settings.WirelessPowerCompCostMulti != 1.0)
            {
                double compMulti = (int)((Settings.Instance.settings.WirelessPowerRangeSmall / 500f));
                if (compMulti < 1) compMulti = 1;
                compMulti = compMulti * Settings.Instance.settings.WirelessPowerCompCostMulti;
                float range = Settings.Instance.settings.WirelessPowerRangeSmall;
                foreach (string subtype in PowerSenderUplink500)
                {
                    MyDefinitionId def = new MyDefinitionId(typeof(MyObjectBuilder_LaserAntenna), subtype);
                    MyLaserAntennaDefinition CBDef = MyDefinitionManager.Static.GetCubeBlockDefinition(def) as MyLaserAntennaDefinition;
                    CBDef.MaxRange = range;
                    CBDef.DisplayNameString = String.Format("Wireless Power Transmitter ({0}m)", range);

                    for (int i = 0; i < CBDef.Components.Length; i++)
                    {
                        CBDef.Components[i].Count = (int)(CBDef.Components[i].Count * compMulti);
                    }
                }

                foreach (string subtype in PowerReceiverUplink500)
                {
                    MyDefinitionId def = new MyDefinitionId(typeof(MyObjectBuilder_LaserAntenna), subtype);
                    MyLaserAntennaDefinition CBDef = MyDefinitionManager.Static.GetCubeBlockDefinition(def) as MyLaserAntennaDefinition;
                    CBDef.MaxRange = range;
                    CBDef.DisplayNameString = String.Format("Wireless Power Receiver ({0}m)", range);

                    for (int i = 0; i < CBDef.Components.Length; i++)
                    {
                        CBDef.Components[i].Count = (int)(CBDef.Components[i].Count * compMulti);
                    }
                }
            }

            if (Settings.Instance.settings.Enable_WirelessPower)
                if (Settings.Instance.settings.WirelessPowerMWSmall != 6f || Settings.Instance.settings.WirelessPowerCompCostMulti != 1.0)
            {
                double compMulti = (int)((Settings.Instance.settings.WirelessPowerMWSmall / 6f));
                if (compMulti < 1) compMulti = 1;
                compMulti = compMulti * Settings.Instance.settings.WirelessPowerCompCostMulti;
                float powerGen = Settings.Instance.settings.WirelessPowerMWSmall;
                float powerDraw = powerGen + (powerGen / 10);

                foreach (string subtype in PowerSenderUplink500)
                {
                    MyDefinitionId def = new MyDefinitionId(typeof(MyObjectBuilder_LaserAntenna), subtype);
                    MyLaserAntennaDefinition CBDef = MyDefinitionManager.Static.GetCubeBlockDefinition(def) as MyLaserAntennaDefinition;
                    CBDef.PowerInputLasing = powerDraw;
                }

                foreach (string subtype in PowerGenerator)
                {
                    MyDefinitionId def = new MyDefinitionId(typeof(MyObjectBuilder_Reactor), subtype);
                    MyReactorDefinition CBDef = MyDefinitionManager.Static.GetCubeBlockDefinition(def) as MyReactorDefinition;
                    CBDef.MaxPowerOutput = powerGen;

                    for (int i = 0; i < CBDef.Components.Length; i++)
                    {
                        CBDef.Components[i].Count = (int)(CBDef.Components[i].Count * compMulti);
                    }
                }
            }
        }

        //Info holder for wireless energy item
        public static itemInfo WirelessEnergy = new itemInfo("Component", "WirelessEnergy", 0.001f, 0.001f);

        //Arrays with subtype names

        //Laser Tethers
        public static string[] TetherUplink500 = new string[] { "LargeBlockTetherUplink500", "SmallBlockTetherUplink500" };
        public static string[] TetherUplink1500 = new string[] { "LargeBlockTetherUplink1500", "SmallBlockTetherUplink1500" };

        //Power Send Tethers
        public static string[] PowerSenderUplink500 = new string[] { "LargeBlockPowerSenderUplink500", "SmallBlockPowerSenderUplink500" };


        //Power Receive Tethers
        public static string[] PowerReceiverUplink500 = new string[] { "SmallBlockPowerReceiverUplink500" };


        //NFC Plates
        public static string[] NFCPad = new string[] { "LargeBlockNFCPad", "SmallBlockNFCPad" };

        //Cargo Teleporters Laser
        public static string[] CargoTeleportTethered = new string[] { "LargeBlockCargoTeleportTethered", "SmallBlockCargoTeleportTethered" };


        //Wireless Power Generator
        public static string[] PowerGenerator = new string[] { "SmallBlockPowerGenerator" };


        //WIP

        public static string[] TetherUplinkCustom = new string[] { "LargeBlockTetherUplinkCustom" };

        public static string[] PowerSenderUplinkCustom = new string[] { "LargeBlockPowerSenderUplinkCustom" };
        public static string[] PowerReceiverUplinkCustom = new string[] { "LargeBlockPowerReceiverUplinkCustom" };
        public static string[] PowerGeneratorCustom = new string[] { "LargeBlockPowerGeneratorCustom" };

    }


    //utility class to store item info in.

    class itemInfo
    {
        public string TypeID = "";
        public string SubtypeID = "";
        public float Mass = 0f;
        public float Volume = 0f;

        public itemInfo(string typeid, string subtypeid, float mass, float volume)
        {
            TypeID = typeid;
            SubtypeID = subtypeid;
            Mass = mass;
            Volume = volume;
        }
    }
}
