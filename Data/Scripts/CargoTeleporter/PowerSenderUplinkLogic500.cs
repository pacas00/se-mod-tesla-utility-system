﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sandbox.Common.ObjectBuilders;
using Sandbox.Common.ObjectBuilders.Definitions;
using Sandbox.Definitions;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.EntityComponents;
using Sandbox.Game.Lights;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Weapons;
using SpaceEngineers.Game.ModAPI;
using System.Timers;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.Entity;
using VRage.Game.ModAPI;
using VRage.Game.ModAPI.Interfaces;
using VRage.Game.ObjectBuilders;
using VRage.ObjectBuilders;
using VRage.Library.Utils;
using VRage.ModAPI;
using VRage.Utils;
using VRage.Game.ObjectBuilders.Definitions;

namespace CargoTeleporter
{
    [MyEntityComponentDescriptor(typeof(MyObjectBuilder_LaserAntenna), new string[] { "LargeBlockPowerSenderUplink500", "SmallBlockPowerSenderUplink500" })]
    public class PowerSenderUplinkLogic500 : MyGameLogicComponent
    {
        MyObjectBuilder_EntityBase ObjectBuilder;
        IMyLaserAntenna LaserAntenna = null;
        public float desiredInput = -1.0f;
        public override void Init(MyObjectBuilder_EntityBase objectBuilder)
        {
            Entity.NeedsUpdate |= MyEntityUpdateEnum.EACH_100TH_FRAME | MyEntityUpdateEnum.BEFORE_NEXT_FRAME;
            ObjectBuilder = objectBuilder;
            LaserAntenna = Entity as IMyLaserAntenna;
            base.Init(objectBuilder);            
        }

        public override MyObjectBuilder_EntityBase GetObjectBuilder(bool copy = false)
        {
            return copy ? ObjectBuilder.Clone() as MyObjectBuilder_EntityBase : ObjectBuilder;
        }

        public override void Close()
        {
            base.Close();
        }

        public override void UpdatingStopped()
        {
            base.UpdatingStopped();
        }

        public void setInputRequired(float f)
        {
            if (f == 0f) f = 0.5f;
            desiredInput = (f / 10) + f;
            if (desiredInput < 0.5f) desiredInput = 0.5f;
        }

        public override void UpdateBeforeSimulation100()
        {
            base.UpdateBeforeSimulation();
            MainLoop();
        }

        public void MainLoop()
        {
            if (LaserAntenna == null) return;
            try
            {
                if (desiredInput < 0f) { desiredInput = Entity.Components.Get<MyResourceSinkComponent>().RequiredInputByType(new MyDefinitionId(typeof(MyObjectBuilder_GasProperties), "Electricity")); }
                Entity.Components.Get<MyResourceSinkComponent>().SetRequiredInputByType(new MyDefinitionId(typeof(MyObjectBuilder_GasProperties), "Electricity"), desiredInput);
                Entity.Components.Get<MyResourceSinkComponent>().SetMaxRequiredInputByType(new MyDefinitionId(typeof(MyObjectBuilder_GasProperties), "Electricity"), desiredInput);
                Entity.Components.Get<MyResourceSinkComponent>().SetInputFromDistributor(new MyDefinitionId(typeof(MyObjectBuilder_GasProperties), "Electricity"), desiredInput, false);
            }
            catch (Exception ex) { Logging.WriteException(ex); }

            try
            {
                if (!(LaserAntenna as IMyFunctionalBlock).Enabled)
                {
                    if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine(LaserAntenna.DisplayNameText + " is powered off");
                    return;
                }
                else
                {
                    if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine(LaserAntenna.DisplayNameText + " is powered on");
                }
            }
            catch (Exception ex)
            {
                Logging.WriteLine(ex.Message);
            }
            try
            {
                if (MyAPIGateway.Session == null)
                {
                    if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("MyAPIGateway.Session is null");
                    return;
                }

                if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("MainRun");
                if (Definitions.PowerSenderUplink500.Contains(LaserAntenna.BlockDefinition.SubtypeId))
                {
                    DateTime compStart = DateTime.Now;

                    //Find Connected Antennas and their grids.
                    if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("CheckInfo");

                    if (commonCodeFragments.IsConnected(LaserAntenna.DetailedInfo))
                    {
                        if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("Connected");
                        //Its Connected
                        VRageMath.BoundingSphereD sphere = new VRageMath.BoundingSphereD(LaserAntenna.TargetCoords, 5.0);

                        IMyLaserAntenna antTarget = MyAPIGateway.Entities.GetEntitiesInSphere(ref sphere)
                            .Where(x => x is IMyLaserAntenna &&
                            ((sphere.Contains((x as IMyLaserAntenna).GetPosition())) == VRageMath.ContainmentType.Contains)
                            ).First() as IMyLaserAntenna;

                        if (Definitions.PowerReceiverUplink500.Contains((antTarget as IMyLaserAntenna).BlockDefinition.SubtypeId))
                        {
                            if (commonCodeFragments.IsConnected((antTarget as IMyLaserAntenna).DetailedInfo))
                            {
                                if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("Found Valid");
                            }
                        }
                        else
                        {
                            if (Settings.Instance.settings.Debug_WirelessPower) Logging.WriteLine("Found Invalid");
                            LaserAntenna.SetTargetCoords("GPS::0:0:0:");
                            LaserAntenna.GetActionWithName("OnOff_Off").Apply(LaserAntenna as IMyCubeBlock);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.WriteLine(ex.Message);
            }

        }

    }
}
