﻿using Sandbox.ModAPI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoTeleporter
{
    public static class Logging
    {
        private static TextWriter writer = null;

        private static List<string> buffer = new List<string>();

        public static bool LoggingReady = false;
        public static void InitialiseLogging()
        {
            try
            {
                writer = MyAPIGateway.Utilities.WriteFileInLocalStorage("CargoTeleport" + ".log", typeof(Logging));
                LoggingReady = true;
                while (buffer.Count > 0)
                {
                    writer.WriteLine(buffer[0]);
                    buffer.RemoveAt(0);
                }
            }
            catch { }

        }

        public static void WriteLine(string s)
        {
            if (!Logging.LoggingReady)
            {
                buffer.Add(DateTime.Now.ToString("[HH:mm:ss] ") + s);
                return;
            }
            try
            {
                writer.WriteLine(DateTime.Now.ToString("[HH:mm:ss] ") + s);
                writer.Flush();
            }
            catch { }
        }

        public static void WriteException(Exception s)
        {
            if (!Logging.LoggingReady)
            {
                buffer.Add(DateTime.Now.ToString("[HH:mm:ss] ") + s.ToString());
                return;
            }
            try
            {
                writer.WriteLine(DateTime.Now.ToString("[HH:mm:ss] ") + s.ToString());
                writer.Flush();
            }
            catch { }
        }

        public static void ShutdownLogging()
        {
            try
            {
                if (writer != null)
                {
                    writer.Flush();
                    writer.Close();
                }
            }
            catch { }

        }
    }
}
