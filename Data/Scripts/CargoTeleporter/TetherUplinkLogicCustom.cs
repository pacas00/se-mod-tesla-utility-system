﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sandbox.Common.ObjectBuilders;
using Sandbox.Common.ObjectBuilders.Definitions;
using Sandbox.Definitions;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.EntityComponents;
using Sandbox.Game.Lights;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Weapons;
using SpaceEngineers.Game.ModAPI;
using System.Timers;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.Entity;
using VRage.Game.ModAPI;
using VRage.Game.ModAPI.Interfaces;
using VRage.Game.ObjectBuilders;
using VRage.ObjectBuilders;
using VRage.Library.Utils;
using VRage.ModAPI;
using VRage.Utils;
using System.IO;

namespace CargoTeleporter
{
    [MyEntityComponentDescriptor(typeof(MyObjectBuilder_LaserAntenna), new string[] { "LargeBlockTetherUplinkCustom" })]
    public class TetherUplinkLogicCustom : MyGameLogicComponent
    {
        MyObjectBuilder_EntityBase ObjectBuilder;
        IMyLaserAntenna LaserAntenna = null;
        public override void Init(MyObjectBuilder_EntityBase objectBuilder)
        {
            Entity.NeedsUpdate |= MyEntityUpdateEnum.EACH_100TH_FRAME;
            ObjectBuilder = objectBuilder;
            LaserAntenna = Entity as IMyLaserAntenna;
            base.Init(objectBuilder);
        }

        public override MyObjectBuilder_EntityBase GetObjectBuilder(bool copy = false)
        {
            return copy ? ObjectBuilder.Clone() as MyObjectBuilder_EntityBase : ObjectBuilder;
        }

        public override void Close()
        {
            base.Close();
        }

        public override void UpdateAfterSimulation()
        {
            base.UpdateAfterSimulation();
        }
        public override void UpdateAfterSimulation10()
        {
            base.UpdateAfterSimulation10();
        }
        public override void UpdateAfterSimulation100()
        {
            base.UpdateAfterSimulation100();
        }
        public override void UpdateBeforeSimulation()
        {
            base.UpdateBeforeSimulation();
        }
        public override void UpdateBeforeSimulation10()
        {
            base.UpdateBeforeSimulation10();
        }
        public override void UpdateOnceBeforeFrame()
        {
            base.UpdateOnceBeforeFrame();
        }
        public override void UpdatingStopped()
        {
            base.UpdatingStopped();
        }

        public override void UpdateBeforeSimulation100()
        {
            base.UpdateBeforeSimulation100();
            if (LaserAntenna == null) return;
            try
            {
                if (!(LaserAntenna as IMyFunctionalBlock).Enabled)
                {
                    if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine(LaserAntenna.DisplayNameText + " is powered off");
                    return;
                }
                else
                {
                    if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine(LaserAntenna.DisplayNameText + " is powered on");
                }
            }
            catch (Exception ex)
            {
                Logging.WriteLine(ex.Message);
            }
            try
            {
                if (MyAPIGateway.Session == null)
                {
                    if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("MyAPIGateway.Session is null");
                    return;
                }

                if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("MainRun");
                if (Definitions.TetherUplinkCustom.Contains(LaserAntenna.BlockDefinition.SubtypeId))
                {
                    long playerId = LaserAntenna.OwnerId;
                    DateTime compStart = DateTime.Now;

                    //Find Connected Antennas and their grids.
                    if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("CheckInfo");

                    if (commonCodeFragments.IsConnected(LaserAntenna.DetailedInfo))
                    {
                        if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("Connected");
                        //Its Connected
                        VRageMath.BoundingSphereD sphere = new VRageMath.BoundingSphereD(LaserAntenna.TargetCoords, 5.0);

                        IMyLaserAntenna antTarget = MyAPIGateway.Entities.GetEntitiesInSphere(ref sphere)
                            .Where(x => x is IMyLaserAntenna &&
                            ((sphere.Contains((x as IMyLaserAntenna).GetPosition())) == VRageMath.ContainmentType.Contains)
                            ).First() as IMyLaserAntenna;

                        if (Definitions.TetherUplinkCustom.Contains((antTarget as IMyLaserAntenna).BlockDefinition.SubtypeId))
                        {
                            if (commonCodeFragments.IsConnected((antTarget as IMyLaserAntenna).DetailedInfo))
                            {
                                if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("Found Valid");
                            }                            
                        }
                        else
                        {
                            if (Settings.Instance.settings.Debug_Tethered) Logging.WriteLine("Found Invalid");
                            LaserAntenna.SetTargetCoords("GPS::0:0:0:");
                            LaserAntenna.GetActionWithName("OnOff_Off").Apply(LaserAntenna as IMyCubeBlock);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.WriteLine(ex.Message);
            }

        }
    }
}
